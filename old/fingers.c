/*

SPEAR is a C program to calculate correlation functions and spectra
Copyright 2013 Frank Uhlig (uhlig.frank@gmail.com)

This file is part of SPEAR.

SPEAR is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPEAR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPEAR.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <unistd.h>
#include <time.h>

void print_fingers ()
{

    printf("\n              L E T'S  G E T  I T  O N ! ! ! \n\n");
    printf("                  .-.\n");
    printf("                 /   \\\n");
    printf("                 |   |\n");
    printf("                 ;-=-;\n");
    printf("                 |   |               .'`\\\n");
    printf("                 | _ |              /   |\n");
    printf("                 |'-'|      .-.    /'. /\n");
    printf("                 |   | .-. /   \\  /   /\n");
    printf("                 |   |/   \\|   | /`=./\n");
    printf("                 ; _.'|   ||-=-|/   /\n");
    printf("                 |`   |-=-||   |`'.;\n");
    printf("      .-\"\"-.     |    |   ||.-.|   |\n");
    printf("     /___   `\\   ;`'-.;.-.;|| ||   |\n");
    printf("     '---;    '._/-._ || ||\\\\_//\"-./\n");
    printf("          `\\     ;   `\\\\_// `\"`    \\\n");
    printf("            \\    .     `\"`,`''--.. |\n");
    printf("             \\             :      `\\\n");
    printf("              `\\'          ,       |\n");
    printf("                \\          .       |\n");
    printf("                 '.       .        ;\n");
    printf("                  `\\    _.        /\n");
    printf("                   |`-;  __ _ ..-'|\n");
    printf("                   |              |\n");
    printf("                   |              |\n");
    printf("                   |              |\n\n");
}

void print_feet ( int lr )
{
    if ( ( lr % 2 ) == 0 )
    {
        printf("   __\n");
        printf("  (  \\\n");
        printf("  )_ |\n");
        printf(" (   /\n");
        printf("()OOOo\n");
    }
    else if ( ( lr % 2 ) == 1 )
    {
        printf(" __\n");
        printf("/  )\n");
        printf("| _(\n");
        printf("\\   )\n");
        printf("oOOO()\n");

    }
}

void print_moving_fingers ()
{

    printf("\n              L E T'S  G E T  I T  O N ! ! ! \n\n");
    sleep ( 1 );
    printf("                  .-.\n");
    // sleep ( 1 );
    printf("                 /   \\\n");
    // sleep ( 1 );
    printf("                 |   |\n");
    sleep ( 1 );
    printf("                 ;-=-;\n");
    // sleep ( 1 );
    printf("                 |   |               .'`\\\n");
    // sleep ( 1 );
    printf("                 | _ |              /   |\n");
    sleep ( 1 );
    printf("                 |'-'|      .-.    /'. /\n");
    // sleep ( 1 );
    printf("                 |   | .-. /   \\  /   /\n");
    // sleep ( 1 );
    printf("                 |   |/   \\|   | /`=./\n");
    sleep ( 1 );
    printf("                 ; _.'|   ||-=-|/   /\n");
    // sleep ( 1 );
    printf("                 |`   |-=-||   |`'.;\n");
    // sleep ( 1 );
    printf("      .-\"\"-.     |    |   ||.-.|   |\n");
    sleep ( 1 );
    printf("     /___   `\\   ;`'-.;.-.;|| ||   |\n");
    // sleep ( 1 );
    printf("     '---;    '._/-._ || ||\\\\_//\"-./\n");
    // sleep ( 1 );
    printf("          `\\     ;   `\\\\_// `\"`    \\\n");
    sleep ( 1 );
    printf("            \\    .     `\"`,`''--.. |\n");
    // sleep ( 1 );
    printf("             \\             :      `\\\n");
    // sleep ( 1 );
    printf("              `\\'          ,       |\n");
    sleep ( 1 );
    printf("                \\          .       |\n");
    // sleep ( 1 );
    printf("                 '.       .        ;\n");
    // sleep ( 1 );
    printf("                  `\\    _.        /\n");
    sleep ( 1 );
    printf("                   |`-;  __ _ ..-'|\n");
    // sleep ( 1 );
    printf("                   |              |\n");
    // sleep ( 1 );
    printf("                   |              |\n");
    // sleep ( 1 );
    printf("                   |              |\n\n");
}
