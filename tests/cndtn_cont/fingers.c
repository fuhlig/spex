#include <stdio.h>
#include <unistd.h>
#include <time.h>

void print_fingers ()
{

    printf("\n              L E T'S  G E T  I T  O N ! ! ! \n\n");
    printf("                  .-.\n");
    printf("                 /   \\\n");
    printf("                 |   |\n");
    printf("                 ;-=-;\n");
    printf("                 |   |               .'`\\\n");
    printf("                 | _ |              /   |\n");
    printf("                 |'-'|      .-.    /'. /\n");
    printf("                 |   | .-. /   \\  /   /\n");
    printf("                 |   |/   \\|   | /`=./\n");
    printf("                 ; _.'|   ||-=-|/   /\n");
    printf("                 |`   |-=-||   |`'.;\n");
    printf("      .-\"\"-.     |    |   ||.-.|   |\n");
    printf("     /___   `\\   ;`'-.;.-.;|| ||   |\n");
    printf("     '---;    '._/-._ || ||\\\\_//\"-./\n");
    printf("          `\\     ;   `\\\\_// `\"`    \\\n");
    printf("            \\    .     `\"`,`''--.. |\n");
    printf("             \\             :      `\\\n");
    printf("              `\\'          ,       |\n");
    printf("                \\          .       |\n");
    printf("                 '.       .        ;\n");
    printf("                  `\\    _.        /\n");
    printf("                   |`-;  __ _ ..-'|\n");
    printf("                   |              |\n");
    printf("                   |              |\n");
    printf("                   |              |\n\n");
}

void print_feet ( int lr )
{
    if ( ( lr % 2 ) == 0 )
    {
        printf("   __\n");
        printf("  (  \\\n");
        printf("  )_ |\n");
        printf(" (   /\n");
        printf("()OOOo\n");
    }
    else if ( ( lr % 2 ) == 1 )
    {
        printf(" __\n");
        printf("/  )\n");
        printf("| _(\n");
        printf("\\   )\n");
        printf("oOOO()\n");

    }
}

void print_moving_fingers ()
{

    printf("\n              L E T'S  G E T  I T  O N ! ! ! \n\n");
    sleep ( 1 );
    printf("                  .-.\n");
    // sleep ( 1 );
    printf("                 /   \\\n");
    // sleep ( 1 );
    printf("                 |   |\n");
    sleep ( 1 );
    printf("                 ;-=-;\n");
    // sleep ( 1 );
    printf("                 |   |               .'`\\\n");
    // sleep ( 1 );
    printf("                 | _ |              /   |\n");
    sleep ( 1 );
    printf("                 |'-'|      .-.    /'. /\n");
    // sleep ( 1 );
    printf("                 |   | .-. /   \\  /   /\n");
    // sleep ( 1 );
    printf("                 |   |/   \\|   | /`=./\n");
    sleep ( 1 );
    printf("                 ; _.'|   ||-=-|/   /\n");
    // sleep ( 1 );
    printf("                 |`   |-=-||   |`'.;\n");
    // sleep ( 1 );
    printf("      .-\"\"-.     |    |   ||.-.|   |\n");
    sleep ( 1 );
    printf("     /___   `\\   ;`'-.;.-.;|| ||   |\n");
    // sleep ( 1 );
    printf("     '---;    '._/-._ || ||\\\\_//\"-./\n");
    // sleep ( 1 );
    printf("          `\\     ;   `\\\\_// `\"`    \\\n");
    sleep ( 1 );
    printf("            \\    .     `\"`,`''--.. |\n");
    // sleep ( 1 );
    printf("             \\             :      `\\\n");
    // sleep ( 1 );
    printf("              `\\'          ,       |\n");
    sleep ( 1 );
    printf("                \\          .       |\n");
    // sleep ( 1 );
    printf("                 '.       .        ;\n");
    // sleep ( 1 );
    printf("                  `\\    _.        /\n");
    sleep ( 1 );
    printf("                   |`-;  __ _ ..-'|\n");
    // sleep ( 1 );
    printf("                   |              |\n");
    // sleep ( 1 );
    printf("                   |              |\n");
    // sleep ( 1 );
    printf("                   |              |\n\n");
}
