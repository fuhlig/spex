/* do we need to normalize each individual correlation function to <a0,a0> before summing them or just afterwards? */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <fftw3.h>
#include "fingers.h"
#include "acf.h"

#define sqr(x) ((x)*(x))
#define MAXSTRLEN 1000
#define SOL 299792458
#define KBOLTZ 1.3806488E-23
#define PLANCK 6.62606957E-34
#define ZERO 0.

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
  ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

int main (int argc, char *argv[])
{
    /* input options */
    int i, j, k;
    int nblcks = 1;
    int dtrng = 1;
    int padd = 1;
    int adjln = 0;
    int nfls, nstps;
    int method = DIRECT_ACF;
    int spec = 0;
    int read_acf = 0;
    int drvtv = 0;
    int crss = 0;
    int dtffst = 0;
    int cntns = 0;
    real thrmlz = -1.;
    real tstp = .5E-15;
    real datln;

    real * acf;
    real * tmpacf;
    real * nrm;
    int blckln;
    int acfln;
    int done = 0;
    int lr = 0;
    int ppf = 0;
    int pps = 0;
    int tstart;
    int tstop;

    char txt[MAXSTRLEN];
    char * dummy;

    FILE * fls;
    FILE * fl;

    tstart = clock ();

    if ( ( argc > 1 ) && ( strstr(argv[1], "are you ready?") != NULL ) ) {
        print_moving_fingers ();
        ppf = 1;
    }
    else if ( ( argc > 1 ) && ( strstr(argv[1], "-v") != NULL ) ) {
        ppf = 1;
    }

    if ( !((fls = fopen("files", "r"))) )
    {
        printf("could not open 'files' file\n");
        exit(1);
    }

    read_input ( &nblcks, &tstp, &dtrng, &nfls, &nstps, &padd, &adjln, &method, &spec, &read_acf, &drvtv, &crss, &dtffst, &thrmlz, &cntns );

    if ( cntns )
        printf("We are calculating a continuous correlation function. Be careful!!! and check code for details :)\n");

    /* just a fix so that we don't need to check conditions below for the progress counter */
    if ( ( ppf ) && ( nfls == 1 ) ) {
        ppf = 0;
        pps = 1;
    }

    real * data;
    real * cdata;

    if ( padd )
        datln = 2 * nstps;
    else
        datln = nstps;

    data = ( real * ) malloc ( datln * sizeof ( real ) );
    
    if ( crss ) 
        cdata = ( real * ) malloc ( datln * sizeof ( real ) );

    for ( i=0; i<datln ; i++ )
        data[i] = ZERO;

    if ( crss )
        for ( i=0; i<datln ; i++ )
            cdata[i] = ZERO;

    // initialize empty acf and nrm data arrays of proper length

    blckln = nstps / nblcks;

    switch ( method )
    {
        case DIRECT_ACF:
            if ( adjln )
                acfln = nstps;
            else
                acfln = blckln;
            break;
        case FFT_ACF:
            /* if padded, we'll throw away the second half of the correlation function */
            acfln = nstps;
            break;
        case NO_ACF:
            // if ( padd )
            //     acfln = 2 * nstps;
            // else
            // check here, below, because later we fft, we only need this size
            acfln = floor ( blckln / 2 ) + 1;
            break;
        default:
            acfln = nstps;
            break;
    }

    acf = ( real * ) malloc ( acfln * sizeof ( real ) );
    tmpacf = ( real * ) malloc ( acfln * sizeof ( real ) );
    nrm = ( real * ) malloc ( acfln * sizeof ( real ) );

    for ( i=0; i<acfln; i++ ) {
        acf[i] = ZERO;
        nrm[i] = ZERO;
        tmpacf[i] = ZERO;
    }

    printf("dtrng: %i, nfls: %i, nblcks: %i\n", dtrng, nfls, nblcks);

    if ( read_acf ) {
        char acfnm[8] = "acf.dat";

        if ( method == NO_ACF ) {
            free ( tmpacf );
            tmpacf = ( real * ) malloc ( nstps * sizeof ( real ) );
            read_data ( tmpacf , 1, nstps, &acfnm[0], dtffst );
        }
        else {
            read_data ( acf , 1, nstps, &acfnm[0], dtffst );
        }
    }
    else {
        for ( i=0; i<nfls; i++ )
        {
            if ( fgets(txt, MAXSTRLEN, fls) == NULL ) {
                printf("Not enough entries in 'files'. Stopped at iteration %i.\n", i);
                exit ( 1 );
            }

            char * fname;
            char * crss_fname;
            // char dummy[MAXSTRLEN];

            // strncpy ( dummy, txt, MAXSTRLEN );

            // printf("%s\n", txt);
            fname = strtok ( txt, " \n" );

            if ( crss ) {
                // crss_fname = strtok ( dummy, " \n" );
                crss_fname = strtok ( NULL, " \n" );
            }
 
            for ( j=0; j<dtrng; j++ )
            {
                // printf("%i\n", j );
                
                // printf("%i\n", j);
                // printf("%s\n", fname);
                // printf("%s\n", crss_fname);

                read_data ( data, j, nstps, fname, dtffst );
                // printf("%f\n", data[0]);

                if ( crss )
                    read_data ( cdata, j, nstps, crss_fname, dtffst);

                // printf("%f\n", cdata[0]);
                // printf("\n\n");

                if ( drvtv ) {
                    real * drv = ( real * ) malloc ( nstps * sizeof ( real ) );
                    drvtv_fv_pnt ( drv, data, nstps, tstp );

                    for ( k=0; k<nstps; k++ ) {
                        data[k] = drv[k];
                    }

                    if ( crss ) {
                        drvtv_fv_pnt ( drv, cdata, nstps, tstp );

                        for ( k=0; k<nstps; k++ ) {
                            cdata[k] = drv[k];
                        }
                    }

                    free ( drv );
                }

                switch ( method )
                {
                    case DIRECT_ACF:
                        if ( crss )
                            corfun_direct ( tmpacf, acfln, data, cdata, nstps, nblcks, cntns );
                        else
                            corfun_direct ( tmpacf, acfln, data, data, nstps, nblcks, cntns );
                        break;
                    case FFT_ACF:
                        if ( crss )
                            corfun_fft ( tmpacf, acfln, data, cdata, nstps, padd );
                        else
                            corfun_fft ( tmpacf, acfln, data, data, nstps, padd );

                        break;
                    case NO_ACF:
                        // check here, we senselessly copy all data, also the zeros, that are already there, considerable overhead for many files
                        // check here, also, if it is okay to average the data before doing the actual fft, or do we need to do the FFTs before and average then

                        // in principle we can also cheat our way here, to get blocked spectrum out of ACF if readacf is != 0

                        blocked_squared_spectrum ( tmpacf, acfln, nblcks, data, nstps );
                        /* currently not available with crss option, because it is not clear to me how exactly to do it */
                        if ( crss ) {
                            printf("Cross-correlation and 'noacf' method not implemented, yet\n");
                            exit ( 1 );
                        }

                        break;
                    default:
                        break;
                }

                if ( pps ) {
                    printf("done: %6.2f\r", ( real ) ( j + 1 ) / dtrng );
                    fflush(stdout);
                }

                // check here, what happens if we do _not_ discard the second half of the spectrum, above change acfln = nstps to acfln = 2 * nstps if ( padd )
                sum_acf ( acf, tmpacf, acfln );
            }

            // done = i / nfls * 100;        
            // if ( done % 10 == 0 ) {
            //     print_feet ( lr );
            //     lr++;
            // }

            if ( ppf ) {
                printf("done: %6.2f\r", ( real ) ( i + 1 ) / nfls);
                fflush(stdout);
            }
        }

        printf("done: %6.2f\n", ( real ) i / nfls);

        if ( ( method == FFT_ACF ) || ( method == DIRECT_ACF ) ) {
            normalize_acf ( acf, acfln );
            output_acf ( acf, acfln, tstp );
        }
        else if ( method == NO_ACF ) {
            int cnt = dtrng * nfls;

            for ( i=0; i<acfln; i++ )
                acf[i] /= ( real ) cnt;
        }

    }

    if ( spec ) {
        fftw_complex * mpltd;

        int spcln;
        int tmpacfln;

        if ( method == NO_ACF ) {
            if ( read_acf )
                blocked_squared_spectrum ( acf, acfln, nblcks, tmpacf, nstps );

            output_spectrum_real ( acf, acfln, tstp, thrmlz );
        }
        else {
            if ( spec == 1 ) {
                spcln = floor ( acfln / 2 ) + 1;
                tmpacfln = acfln;
            }
            else {
                spcln = floor ( spec / 2 ) + 1;
                tmpacfln = spec;
            }

            // mpltd = ( real * ) malloc ( spcln * sizeof ( real ) );
            mpltd = fftw_malloc ( sizeof ( fftw_complex ) * spcln );

            get_spectrum ( mpltd, acf, tmpacfln );

            output_spectrum ( mpltd, spcln, tstp, thrmlz );

            fftw_free(mpltd);
        }

    }

    free(data);
    if ( crss )
        free ( cdata );
    free(acf);
    free(tmpacf);
    free(nrm);

    fclose(fls);

    tstop = clock ();

    real dt = ( real ) ( tstop - tstart ) / CLOCKS_PER_SEC;
    printf("It took me %5.2f s to do all the work for you!\n", dt );

    if ( dt > 60. )
        printf("That took long. What the hell are you doing?\n");
}

void corfun_direct ( real * acf, int acfln, real * data, real * cdata, int nstps, int nblcks, int cntns )
{
    /* this is a basic copy of what is given in Allen/Tildesley */

    int crss = 0;
    int stpln;
    int t0, tt0, t;
    int tmp, i;
    int tt0max;

    real a0;

    real nrm[acfln];

    for ( i=0; i<acfln; i++ ) {
        nrm[i] = ZERO;
        acf[i] = ZERO;
    }

    if ( data != cdata )
        crss = 1;

    int tcor = acfln;

    stpln = nstps / nblcks;

    real ctmp;

    /* doing the if in the inner loop gives us for large files (50000 entries) about 2s slow-down */

    for ( t0=0; t0<nstps; t0 += stpln ) {
        a0 = data[t0];

        tmp = t0 + tcor;
        tt0max = min( nstps, tmp );

        ctmp = a0;

        for ( tt0=t0; tt0<tt0max; tt0++ ) {
            t = tt0 - t0;

            // if ( 1 & cntns )
            if ( cntns )
                ctmp = ctmp * cdata[tt0];
            else 
                ctmp = a0 * cdata[tt0];

            acf[t] = acf[t] + ctmp;
            nrm[t] = nrm[t] + 1.0;

            /* can we do this with integers? what exactly will happen, when we convert floats to integers? */
            /* currently the below will fail, because not everything beyond break-point will be assigned correctly... */

            /* if ( ( ctmp < 0.5 ) )
             *     break;
             */

        }

    }

    for ( t=0; t<acfln; t++ )
        acf[t] = acf[t] / nrm[t];

}

void corfun_fft ( real * acf, int acfln, real * data, real * cdata, int nstps, int padd )
{
    int stpln;
    int t0, tt0, t;
    int tmp, i;
    int tt0max;
    int fftln;
    int datln;
    int crss = 0;

    fftw_plan plan_forward;
    fftw_plan plan_backward;
    fftw_complex * fwout;
    fftw_complex * crssfwout;
    fftw_complex * bwin;
    real * bwout;

    real a0;

    if ( data != cdata )
        crss = 1;

    if ( padd )
        datln = 2 * nstps;
    else
        datln = nstps;

    real tmpacf[datln];

    for ( i=0; i<datln; i++ )
        tmpacf[i] = ZERO;

    for ( i=0; i<acfln; i++ )
        acf[i] = ZERO;

    /* check here and maybe reduce it by an given input... */

    fftln = floor ( datln / 2 ) + 1;

    fwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );
    if ( crss )
        crssfwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );
    bwin = fftw_malloc ( sizeof ( fftw_complex ) * fftln );

    /* verify somehow that the complex part is always zeros */

    plan_forward = fftw_plan_dft_r2c_1d ( datln, data, fwout, FFTW_ESTIMATE );
    fftw_execute ( plan_forward );

    if ( crss ) {
        fftw_destroy_plan ( plan_forward );
        plan_forward = fftw_plan_dft_r2c_1d ( datln, cdata, crssfwout, FFTW_ESTIMATE );
        fftw_execute ( plan_forward );
    }

    /* produce square of absolute of complex numbers (square modulus) */
    /* find out why it does not matter that we don't scale it back before getting the square modulus */

    if ( crss )
        cmplx_conj_product ( bwin, fwout, crssfwout, fftln );
    else
        square_modulus ( bwin, fwout, fftln );

    plan_backward = fftw_plan_dft_c2r_1d ( datln, bwin, tmpacf, FFTW_ESTIMATE );

    fftw_execute ( plan_backward );

    /* scale data */
    /* normalize_fftw is double as expensive as the loop below, we need only half */

    // normalize_fftw ( tmpacf, datln );

    for ( i=0; i<acfln; i++ )
        tmpacf[i] /= datln;

    sum_acf ( acf, tmpacf, acfln );

    /* interestingly http://fy.chalmers.se/OLDUSERS/ahlstrom/fft_ac.pdf disagrees */
    if ( padd )
        normalize_acf_fft_padd ( acf, acfln );

    /* what if it's not padded? we should be fine, i think */

    fftw_destroy_plan ( plan_forward );
    fftw_destroy_plan ( plan_backward );

    fftw_free ( fwout );
    if ( crss )
        fftw_free ( crssfwout );
    fftw_free ( bwin );

    fftw_cleanup ();

}

/* read in data provided in file */
void read_data ( real * data, int dtrng, int nstps, char * txt, int offset )
{
    char rdln[MAXSTRLEN];
    char * vl;
    char * fname;

    FILE * fl;

    int i, j;

    fname = strtok ( txt, "\n");

    if ( !(( fl = fopen( txt, "r" ))) )
    {
        printf("could not open '%s' file\n", txt);
        exit(1);
    }

    for ( i=0; i<offset; i ++ ) {
        if ( fgets(rdln, MAXSTRLEN, fl) == NULL ) {
            printf("Not enough data points in '%s'. Stopped at iteration %i\n", txt, i);
            exit ( 1 );
        }
    }

    for ( i=0; i<nstps; i++ )
    {
        if ( fgets(rdln, MAXSTRLEN, fl) == NULL ) {
            printf("Not enough data points in '%s'. Stopped at iteration %i\n", txt, i);
            exit ( 1 );
        }

        vl = strtok (rdln, " \n");

        /* check here again, it should work, but somehow i am confused right now */
        for ( j=1; j<=dtrng; j++ )
            vl = strtok (NULL, " \n");

        if ( vl == NULL ) {
            printf("Not enough columns in data file '%s'\n", txt);
            exit ( 1 );
        }

        data[i] = atof ( vl );
    }

    fclose(fl);
}

/* routine from allen/tildesley to calculate correlation function */

void read_input( int * nblcks, real * tstp, int * dtrng, int * nfls, int * nstps, int * padd, int * adjln, int * method, int * spec, int * read_acf, int * drvtv, int * crss, int * dtffst, real * thrmlz, int * cntns )
{
    FILE * datei;
    char * variable;
    char * value;
    char text[MAXSTRLEN];

    if ( !((datei = fopen("control", "r"))) )
    {
        printf("could not open 'control' file\n");
        exit(1);
    }

    while(fgets(text, MAXSTRLEN, datei) != NULL)
    {
        variable = strtok (text, " ");
        value = strtok (NULL, " ");

        if ( strstr(variable, "nblcks") != NULL )
        {
            *nblcks = atol(value);
        }
        else if ( strstr(variable, "tstp") != NULL )
        {
            *tstp = atof(value);
        }
        else if ( strstr(variable, "dtrng") != NULL )
        {
            *dtrng = atol(value);
        }
        else if ( strstr(variable, "nfls") != NULL )
        {
            *nfls = atol(value);
        }
        else if ( strstr(variable, "nstps") != NULL )
        {
            *nstps = atol(value);
        }
        else if ( strstr(variable, "padd") != NULL )
        {
            *padd = atol(value);
        }
        else if ( strstr(variable, "adjln") != NULL )
        {
            *adjln = atol(value);
        }
        else if ( strstr(variable, "method") != NULL )
        {
            if ( strstr ( value, "direct" ) != NULL )
                *method = DIRECT_ACF;
            else if ( strstr ( value, "fft" ) != NULL )
                *method = FFT_ACF;
            else if ( strstr ( value, "noacf" ) != NULL )
                *method = NO_ACF;
        }
        else if ( strstr(variable, "spec") != NULL )
        {
            *spec = atol(value);
        }
        else if ( strstr(variable, "read_acf") != NULL )
        {
            *read_acf = atol(value);
        }
        else if ( strstr(variable, "drvtv") != NULL )
        {
            *drvtv = atol(value);
        }
        else if ( strstr(variable, "crss") != NULL )
        {
            *crss = atol(value);
        }
        else if ( strstr(variable, "dtffst") != NULL )
        {
            *dtffst = atol(value);
        }
        else if ( strstr(variable, "thrmlz") != NULL )
        {
            *thrmlz = atof(value);
        }
        else if ( strstr(variable, "cntns") != NULL )
        {
            *cntns = atol(value);
        }
    }

    fclose(datei);

}

void sum_acf ( real * acf, real * tmpacf, int len)
{
    int i;

    for ( i=0; i<len; i++ )
        acf[i] += tmpacf[i];

    return;
}

void output_acf ( real * acf, int acfln, real tstp )
{
    FILE * fout;
    int i;

    if ( !((fout = fopen ( "acf.dat", "w" ))) )
    {
        printf("could not open 'acf.dat' file\n");
        exit(1);
    }

    for ( i=0; i<acfln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", i*tstp, acf[i]);

    fclose(fout);
}

void normalize_acf ( real * acf, int acfln )
{
    int i;
    real a0;

    a0 = acf[0];

    /* get proper normalization with respect to average and stddev, see TODO file*/
    for ( i=0; i<acfln; i++ )
        acf[i] /= a0;

}

void square_modulus ( fftw_complex * out, fftw_complex * in, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i][0] = sqr ( in[i][0] ) + sqr ( in[i][1] );
        out[i][1] = ZERO;
    }

}

void square_modulus_real ( real * out, fftw_complex * in, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i] = sqr ( in[i][0] ) + sqr ( in[i][1] );
    }

}

void cmplx_conj_product ( fftw_complex * out, fftw_complex * in1, fftw_complex * in2, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i][0] = in1[i][0] * in2[i][0] + in1[i][1] * in2[i][1];
        out[i][1] = in1[i][0] * in2[i][1] - in2[i][0] * in1[i][1];
        // printf("%f\n", out[i][1]);
    }

}

void normalize_fftw ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] /= len;

}

void collect_norm ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] += 1.;

}

void normalize_acf_fft_padd ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] /= ( len - i );

}

void sum_fft ( fftw_complex * fft, fftw_complex * tmpfft, int len)
{
    int i;

    for ( i=0; i<len; i++ ) {
        fft[i][0] += tmpfft[i][0];
        fft[i][1] += tmpfft[i][1];
    }

    return;
}

void get_spectrum ( fftw_complex * mpltd, real * acf, int acfln )
{
    int i;
    int fftln;

    fftw_complex * fwout;
    fftw_plan plan_forward;
    
    fftln = floor ( acfln / 2 ) + 1;
    // fwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );

    plan_forward = fftw_plan_dft_r2c_1d ( acfln, acf, mpltd, FFTW_ESTIMATE );

    fftw_execute ( plan_forward );

    // for ( i=0; i<fftln; i++ ) {
    //     mpltd[i] = fwout[i][0];
    // }

    fftw_destroy_plan ( plan_forward );
    // fftw_free ( fwout );
    fftw_cleanup ();

}

void output_spectrum ( fftw_complex * mpltd, int spcln, real dt, real thrmlz )
{
    int i;
    FILE * fout;

    real fs;
    real df;

    fs = .5 / dt;
    df = fs / spcln;
    
    if ( !((fout = fopen ( "spectrum.dat", "w" ))) )
    {
        printf("could not open 'spectrum.dat' file\n");
        exit(1);
    }

    if ( thrmlz > 0.0 ) {
        real wvn2frq;

        wvn2frq = SOL * 100.;

        for ( i=0; i<spcln; i++ )
            mpltd[i][0] *= ( 1. - exp ( -1. / KBOLTZ / thrmlz * i * df * PLANCK ) );
    }

    for ( i=0; i<spcln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", ( real ) i * df / SOL / 100. , mpltd[i][0]);
        // fprintf(fout, "%20.10e\n", mpltd[i]);

    fclose(fout);
}

void output_spectrum_real ( real * mpltd, int spcln, real dt, real thrmlz )
{
    int i;
    FILE * fout;

    real fs;
    real df;

    fs = .5 / dt;
    df = fs / spcln;
    
    if ( !((fout = fopen ( "spectrum.dat", "w" ))) )
    {
        printf("could not open 'spectrum.dat' file\n");
        exit(1);
    }

    if ( thrmlz > 0. ) {
        real wvn2frq;

        wvn2frq = SOL * 100.;

        for ( i=0; i<spcln; i++ )
            mpltd[i] *= ( 1. - exp ( -1. / KBOLTZ / thrmlz * i * df * PLANCK ) );
    }

    for ( i=0; i<spcln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", ( real ) i * df / SOL / 100. , mpltd[i]);
        // fprintf(fout, "%20.10e\n", mpltd[i]);

    fclose(fout);
}

void drvtv_fv_pnt ( real * drv, real * data, int len, real h )
{
    int i;
    int fvp;

    real dt;

    /* check here if we need to do something about the units */
    dt = h / 1.E-15;

    fvp = len - 2;

    for ( i=2; i<fvp; i++ ) {
        drv[i] = -1. * data[i+2] + 8. * data[i+1] - 8. * data[i-1] + data[i-2];
        drv[i] /= ( 12. * dt );
        // drv[i] /= 12.;
    }

    drv[1] = data[2] - data[0];
    drv[1] /= ( 2. * dt );
    // drv[1] /= 2.;

    drv[len-2] = data[len-1] - data[len-3];
    drv[len-2] /= ( 2. * dt );
    // drv[len-2] /= 2.;

    drv[0] = data[1] - data[0];
    drv[0] /=  dt;
    // drv[0] /= 1;

    drv[len-1] = data[len-1] - data[len-2];
    drv[len-1] /= dt;
    // drv[len-1] /= 1;

}

void blocked_squared_spectrum ( real * spcout, int acfln, int nblcks, real * data, int datln )
{
    int i;
    int cntfft = 0;
    int blckln;
    int fft_blckln;
    int tmpln;

    real tmpout[acfln];
    fftw_complex * fwout;
    fftw_plan plan_forward;

    blckln = floor ( datln / nblcks );
    fft_blckln = floor ( blckln / 2 ) + 1;

    fwout = fftw_malloc ( sizeof ( fftw_complex ) * fft_blckln );

    /* check here and add individual padding? (dunt make no sense, IMHO) */
    for ( i=0; i<acfln; i++ )
        tmpout[i] = ZERO;

    for ( i=0; i<datln; i += blckln ) {
        tmpln = datln - i;

        if ( tmpln < blckln )
            break;

        plan_forward = fftw_plan_dft_r2c_1d ( blckln, &(data[i]), fwout, FFTW_ESTIMATE );

        fftw_execute ( plan_forward );

        square_modulus_real ( tmpout, fwout, acfln );

        sum_acf ( spcout, tmpout, acfln );

        fftw_destroy_plan ( plan_forward);

        cntfft++;

    }

    fftw_free ( fwout );
    fftw_cleanup ();

    for ( i=0; i<acfln; i++ )
        spcout[i] /= ( real ) cntfft;

}
