#include <fftw3.h>

typedef enum methods_e
{
    DIRECT_ACF=1,
    FFT_ACF,
    NO_ACF,
} methods_t;

// typedef float real;
typedef double real;
void read_data (real * data, int dtrng, int nstps, char * txt, int offset );
void read_input(int * nblcks, real * tstp, int * dtrng, int * nfls, int * nstps, int * padd, int * adjln, int * method, int * spec, int * read_acf, int * drvtv, int * crss, int * dtffst, real * thrmlz, int * cntns );
void corfun_direct ( real * acf, int acfln, real * data, real * cdata, int nstps, int nblcks, int cntns );
void corfun_fft (real * acf, int acfln, real * data, real * cdata, int nstps, int padd );
void sum_acf ( real * acf, real * tmpacf, int len);
void output_acf ( real * acf, int acfln, real tstp );
void normalize_acf ( real * acf, int acfln );
void copy_data ( real * out, real * in, int start, int len );
void square_modulus ( fftw_complex * out, fftw_complex * in, int len );
void square_modulus_real ( real * out, fftw_complex * in, int len );
void cmplx_conj_product ( fftw_complex * out, fftw_complex * in1, fftw_complex * in2, int len );
void normalize_fftw ( real * data, int len );
void collect_norm ( real * data, int len );
void normalize_acf_fft_padd ( real * data, int len );
void sum_fft ( fftw_complex * fft, fftw_complex * tmpfft, int len);
void print_fingers ();
void print_feet ( int lr );
void get_spectrum ( fftw_complex * mpltd, real * acf, int acfln );
void output_spectrum ( fftw_complex * mpltd, int spcln, real dt, real thrmlz );
void output_spectrum_real ( real * mpltd, int spcln, real dt, real thrmlz );
void drvtv_fv_pnt ( real * drv, real * data, int len, real h );
void blocked_squared_spectrum ( real * spcout, int acfln, int nblcks, real * data, int datln );
