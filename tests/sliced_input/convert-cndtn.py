#!/usr/bin/env python

import sys
import numpy as np

dat = np.loadtxt('H2O_cndtn.dat')

nsnaps = len(dat)

files = [
        'dip-2.dat',
        'dip-3.dat',
        'dip-4.dat',
        'dip-5.dat',
        'dip-6.dat',
        'dip-7.dat',
        'dip-8.dat',
        'dip-9.dat',
        'dip-10.dat',
        'dip-11.dat',
        'dip-12.dat',
        'dip-13.dat',
        'dip-14.dat',
        'dip-15.dat',
        'dip-16.dat',
        'dip-17.dat',
        'dip-18.dat',
        'dip-19.dat',
        'dip-20.dat',
        'dip-21.dat',
        'dip-22.dat',
        'dip-23.dat',
        'dip-24.dat',
        'dip-25.dat',
        'dip-26.dat',
        'dip-27.dat',
        'dip-28.dat',
        'dip-29.dat',
        'dip-30.dat',
        'dip-31.dat',
        'dip-32.dat',
        'dip-33.dat',
        'dip-34.dat',
        'dip-35.dat',
        'dip-36.dat',
        'dip-37.dat',
        'dip-38.dat',
        'dip-39.dat',
        'dip-40.dat',
        'dip-41.dat',
        'dip-42.dat',
        'dip-43.dat',
        'dip-44.dat',
        'dip-45.dat',
        'dip-46.dat',
        'dip-47.dat',
        'dip-48.dat',
        'dip-49.dat',
        'dip-50.dat',
        'dip-51.dat',
        'dip-52.dat',
        'dip-53.dat',
        'dip-54.dat',
        'dip-55.dat',
        'dip-56.dat',
        'dip-57.dat',
        'dip-58.dat',
        'dip-59.dat',
        'dip-60.dat',
        'dip-61.dat',
        'dip-62.dat',
        'dip-63.dat',
        'dip-64.dat',
        ]

fslc = open('slices.dat', 'w')
nfls = dat.shape[1]
    
for col in range(nfls):

    fllflld = False
    written = False
    
    slc = []
    
    beg = -1
    end = -1

    for i in range(nsnaps):
        if dat[i,col] == 1:
            nffd = True
        else:
            nffd = False
    
        if not fllflld and nffd:
            beg = i
            fllflld = True
    
        elif not fllflld and not nffd:
            continue
    
        elif fllflld and nffd:
            continue
    
        elif fllflld and not nffd:
            end = i
            fllflld = False
    
        if beg != -1 and end != -1:
            slc.append([(beg,end)])
    
            #if not written:
            #    fslc.write("%20s |" %files[col]);
            #    written = True

            #fslc.write("%i:%i|" %(beg, end))
            fslc.write("%30s %10i %10i\n" %(files[col], beg, end))

            beg = -1
            end = -1

    #if written:
    #    fslc.write("\n")

    sys.stdout.write('done with file %i / %i\r' %(col, nfls))
    sys.stdout.flush()

fslc.close()
