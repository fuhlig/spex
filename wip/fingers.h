/*

SPEAR is a C program to calculate correlation functions and spectra
Copyright 2013 Frank Uhlig (uhlig.frank@gmail.com)

This file is part of SPEAR.

SPEAR is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPEAR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPEAR.  If not, see <http://www.gnu.org/licenses/>.

*/

void print_fingers ();
void print_feet ( int lr );
void print_moving_fingers ();
