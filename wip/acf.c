/*

SPEAR is a C program to calculate correlation functions and spectra
Copyright 2013 Frank Uhlig (uhlig.frank@gmail.com)

This file is part of SPEAR.

SPEAR is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPEAR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPEAR.  If not, see <http://www.gnu.org/licenses/>.

*/

/* do we need to normalize each individual correlation function to <a0,a0> before summing them or just afterwards? */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <fftw3.h>
#include "errors.h"
#include "fingers.h"
#include "acf.h"

#define sqr(x) ((x)*(x))
#define MAXSTRLEN 500000
#define SOL 299792458
#define KBOLTZ 1.3806488E-23
#define PLANCK 6.62606957E-34
#define ZERO 0.

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
  ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

int main (int argc, char *argv[])
{
    /* input options */
    int i, j, k;
    int nblcks = 1;
    int dtrng = 1;
    int padd = 0;
    int adjln = 0;
    int nfls = 0;
    int nstps = 0;
    int method = DIRECT_ACF;
    int spec = 0;
    int read_acf = 0;
    int drvtv = 0;
    int crss = 0;
    int dtffst = 0;
    int cntns = 0;
    int slcd = 0;
    /* check here, for some reason datln was a real before */
    int datln;
    int strd = 1;
    real thrmlz = -1.;
    real tstp = .5E-15;
    /* check here and find a better way of doing it */
    real ctff = -10000.;

    real * acf;
    real * tmpacf;
    real * nrm;
    int blckln;
    int acfln;
    int done = 0;
    int lr = 0;
    int ppf = 0;
    int pps = 0;
    int tstart;
    int tstop;
    int fxdfmt = 0;

    char txt[MAXSTRLEN];
    char * dummy;

    FILE * fls;
    FILE * fl;

    tstart = clock ();

    if ( ( argc > 1 ) && ( strstr(argv[1], "are you ready?") != NULL ) ) {
        print_moving_fingers ();
        ppf = 1;
    }
    else if ( ( argc > 1 ) && ( strstr(argv[1], "-v") != NULL ) ) {
        ppf = 1;
    }

    if ( !((fls = fopen("files", "r"))) )
    {
        print_error ( FILE_NOT_FOUND, "files", __FILE__, __LINE__);
    }

    read_input ( &nblcks, &tstp, &dtrng, &nfls, &nstps, &padd, &adjln, &method, &spec, &read_acf, &drvtv, &crss, &dtffst, &thrmlz, &cntns, &slcd, &ctff, &fxdfmt, &strd );

    check_malformed_input ( nblcks, tstp, dtrng, nfls, nstps, padd, adjln, method, spec, read_acf, drvtv, crss, dtffst, thrmlz, cntns, slcd, ctff, fxdfmt, strd );

    if ( cntns )
        print_warning ( YOU_KNOW_WHAT, "We are calculating a continuous correlation function. Be careful!!! and check code for details :)" );

    /* just a fix so that we don't need to check conditions below for the progress counter */
    if ( ( ppf ) && ( nfls == 1 ) ) {
        ppf = 0;
        pps = 1;
    }

    real * data;
    real * cdata;

    nstps /= strd;

    if ( padd )
        datln = 2 * nstps;
    else
        datln = nstps;

    data = ( real * ) malloc ( datln * sizeof ( real ) );
    
    if ( crss ) 
        cdata = ( real * ) malloc ( datln * sizeof ( real ) );

    for ( i=0; i<datln ; i++ )
        data[i] = ZERO;

    if ( crss )
        for ( i=0; i<datln ; i++ )
            cdata[i] = ZERO;

    // initialize empty acf and nrm data arrays of proper length

    blckln = nstps / nblcks;

    switch ( method )
    {
        case DIRECT_ACF:
            if ( adjln )
                acfln = nstps;
            else
                acfln = blckln;
            break;
        case FFT_ACF:
            /* if padded, we'll throw away the second half of the correlation function */
            acfln = nstps;
            break;
        case NO_ACF:
            // if ( padd )
            //     acfln = 2 * nstps;
            // else
            // check here, below, because later we fft, we only need this size
            acfln = floor ( blckln / 2 ) + 1;
            break;
        default:
            acfln = nstps;
            break;
    }

    acf = ( real * ) malloc ( acfln * sizeof ( real ) );
    tmpacf = ( real * ) malloc ( acfln * sizeof ( real ) );
    nrm = ( real * ) malloc ( acfln * sizeof ( real ) );

    for ( i=0; i<acfln; i++ ) {
        acf[i] = ZERO;
        nrm[i] = ZERO;
        tmpacf[i] = ZERO;
    }

    printf("dtrng: %i, nfls: %i, nblcks: %i\n", dtrng, nfls, nblcks);

    if ( ( method == DIRECT_ACF ) && ( !( slcd ) ) )
        generate_nrm ( nrm, acfln, nstps, nblcks, acfln );

    if ( read_acf ) {
        char acfnm[8] = "acf.dat";

        if ( method == NO_ACF ) {
            free ( tmpacf );
            tmpacf = ( real * ) malloc ( nstps * sizeof ( real ) );
            read_data ( tmpacf , 1, nstps, &acfnm[0], dtffst );
        }
        else {
            read_data ( acf , 1, nstps, &acfnm[0], dtffst );
        }
    }
    else {
        for ( i=0; i<nfls; i++ )
        {
            if ( fgets(txt, MAXSTRLEN, fls) == NULL ) {
                print_error ( NOT_ENOUGH_DATA, "files", __FILE__, __LINE__ );
                // printf("Not enough entries in 'files'. Stopped at iteration %i.\n", i);
            }

            char * fname;
            char * crss_fname;
            // char slcdmm[MAXSTRLEN];

            // strncpy ( slcdmm, txt, MAXSTRLEN );
            // printf("%s\n", txt);

            fname = strtok ( txt, " \n" );

            if ( crss ) {
                // crss_fname = strtok ( dummy, " \n" );
                crss_fname = strtok ( NULL, " \n" );
            }

            int tmpacfln;

            if ( slcd )
            {
                char * start, *stop;
                int tmp;

                start = strtok ( NULL, " \n" );
                if ( start == NULL )
                    print_error ( INCOMPLETE_INPUT, "Not enough slice entries in files" , __FILE__, __LINE__);

                stop = strtok ( NULL, " \n" );
                if ( stop == NULL )
                    print_error ( INCOMPLETE_INPUT, "Not enough slice entries in files", __FILE__, __LINE__);

                dtffst = atol(start);
                tmp = atol(stop);
                nstps = tmp - dtffst;

                nblcks = nstps;
                tmpacfln = nstps;

                generate_nrm ( nrm, tmpacfln, nstps, nblcks, tmpacfln );

                // printf("file: %s start: %i stop: %i\n", fname, dtffst, tmp);

            }
            else
            {
                tmpacfln = acfln;
            }
 
            for ( j=0; j<dtrng; j++ )
            {
                // printf("%i\n", j );
                
                // printf("%i\n", j);
                // printf("%s\n", fname);
                // printf("%s\n", crss_fname);

                if ( fxdfmt )
                    read_data_fxd_fmt ( data, j, strd*nstps, fname, dtffst, fxdfmt, strd );
                else
                    read_data ( data, j, nstps, fname, dtffst );
                // printf("%f\n", data[0]);

                if ( crss )
                    if ( fxdfmt )
                        read_data_fxd_fmt ( cdata, j, strd*nstps, crss_fname, dtffst, fxdfmt, strd );
                    else
                        read_data ( cdata, j, nstps, crss_fname, dtffst);

                // printf("%f\n", cdata[0]);
                // printf("\n\n");

                if ( drvtv ) {
                    if ( nstps <= 1 )
                        print_error ( FATAL, "Cannot calculate derivative with only one point. Please check your input!", __FILE__, __LINE__);

                    real * drv = ( real * ) malloc ( nstps * sizeof ( real ) );
                    drvtv_fv_pnt ( drv, data, nstps, tstp );

                    for ( k=0; k<nstps; k++ ) {
                        data[k] = drv[k];
                    }

                    if ( crss ) {
                        drvtv_fv_pnt ( drv, cdata, nstps, tstp );

                        for ( k=0; k<nstps; k++ ) {
                            cdata[k] = drv[k];
                        }
                    }

                    free ( drv );
                }

                switch ( method )
                {
                    case DIRECT_ACF:
                        if ( crss )
                            corfun_direct ( tmpacf, tmpacfln, data, cdata, nstps, nblcks, cntns, nrm, ctff );
                        else
                            corfun_direct ( tmpacf, tmpacfln, data, data, nstps, nblcks, cntns, nrm, ctff );
                        break;
                    case FFT_ACF:
                        if ( crss )
                            corfun_fft ( tmpacf, tmpacfln, data, cdata, nstps, padd );
                        else
                            corfun_fft ( tmpacf, tmpacfln, data, data, nstps, padd );

                        break;
                    case NO_ACF:
                        // check here, we senselessly copy all data, also the zeros, that are already there, considerable overhead for many files
                        // check here, also, if it is okay to average the data before doing the actual fft, or do we need to do the FFTs before and average then

                        // in principle we can also cheat our way here, to get blocked spectrum out of ACF if readacf is != 0

                        blocked_squared_spectrum ( tmpacf, tmpacfln, nblcks, data, nstps );
                        /* currently not available with crss option, because it is not clear to me how exactly to do it */
                        if ( crss ) {
                            // printf("Cross-correlation and 'noacf' method not implemented, yet\n");
                            print_error ( NOT_IMPLEMENTED, "Cross-correlation and 'NOACF' method", __FILE__, __LINE__ );
                        }

                        break;
                    default:
                        break;
                }

                if ( pps ) {
                    printf("done: %6.2f\r", ( real ) ( j + 1 ) / dtrng );
                    fflush(stdout);
                }

                // check here, what happens if we do _not_ discard the second half of the spectrum, above change acfln = nstps to acfln = 2 * nstps if ( padd )
                sum_acf ( acf, tmpacf, tmpacfln );
            }

            // done = i / nfls * 100;        
            // if ( done % 10 == 0 ) {
            //     print_feet ( lr );
            //     lr++;
            // }

            if ( ppf ) {
                printf("done: %6.2f\r", ( real ) ( i + 1 ) / nfls);
                fflush(stdout);
            }
        }

        printf("done: %6.2f\n", ( real ) i / nfls);

        /* check normalization again, again and again */

        if ( ( method == FFT_ACF ) || ( method == DIRECT_ACF ) ) {
            /* output not normalized acf as well and decide which one we want to use */
            normalize_acf ( acf, acfln );

            // normalize_acf_nfls ( acf, acfln, nfls*dtrng );
            output_acf ( acf, acfln, tstp );
        }
        else if ( method == NO_ACF ) {
            int cnt = dtrng * nfls;

            for ( i=0; i<acfln; i++ )
                acf[i] /= ( real ) cnt;
        }

    }

    if ( spec ) {
        fftw_complex * mpltd;

        int spcln;
        int tmpacfln;

        if ( method == NO_ACF ) {
            if ( read_acf )
                blocked_squared_spectrum ( acf, acfln, nblcks, tmpacf, nstps );

            output_spectrum_real ( acf, acfln, tstp, thrmlz );
        }
        else {
            if ( spec == 1 ) {
                spcln = floor ( acfln / 2 ) + 1;
                tmpacfln = acfln;
            }
            else {
                spcln = floor ( spec / 2 ) + 1;
                tmpacfln = spec;
            }

            // mpltd = ( real * ) malloc ( spcln * sizeof ( real ) );
            mpltd = fftw_malloc ( sizeof ( fftw_complex ) * spcln );

            get_spectrum ( mpltd, acf, tmpacfln );

            output_spectrum ( mpltd, spcln, tstp, thrmlz );

            fftw_free(mpltd);
        }

    }

    free(data);
    if ( crss )
        free ( cdata );
    free(acf);
    free(tmpacf);
    free(nrm);

    fclose(fls);

    tstop = clock ();

    real dt = ( real ) ( tstop - tstart ) / CLOCKS_PER_SEC;
    printf("It took me %5.2f s to do all the work for you!\n", dt );

    if ( dt > 60. )
        printf("That took long. What the hell are you doing?\n");
}

void corfun_direct ( real * acf, int acfln, real * data, real * cdata, int nstps, int nblcks, int cntns, real * nrm, real ctff )
{
    /* this is a basic copy of what is given in Allen/Tildesley */

    int crss = 0;
    int stpln;
    int t0, tt0, t;
    int tmp, i;
    int tt0max;

    real a0;
    real mean;
    real cmean;
    real var;
    real cvar;

    // real nrm[acfln];

    for ( i=0; i<acfln; i++ ) {
        // nrm[i] = ZERO;
        acf[i] = ZERO;
    }

    /* check here, this is strictly speaking only correct if we use the data for only one correlation function */
    /* otherwise we need to recalculate the mean and variance independently for each segment of the data */
    /* holds for second-order stationary processes, better check with augmented Dickey-Fuller test (http://en.wikipedia.org/wiki/Dickey%E2%80%93Fuller_test) */
    /* if second-order startionary process then mean and variance are time-independent */
    /* on short time scales as in AIMD it could actually matter... */

    /* in general it might just be wrong to apply this statistically correct form of the autocorrelation function */
    /* because it is not necessarily what would be measured.... or is it? */
    /* it will stay commented until we know exactly how to deal with it */

    /*
     * mean = get_mean ( data, nstps );
     * var = get_variance_with_mean ( data, nstps, mean );

     * subtract_value ( data, nstps, mean );
     */

    if ( data != cdata ) {
        crss = 1;

        /*
         * cmean = get_mean ( cdata, nstps );
         * cvar = get_variance_with_mean ( data, nstps, mean );
         *
         * subtract_value ( cdata, nstps, cmean );
         */
    }
    else {
        cmean = mean;
        cvar = var;
    }

    /* check again if adjln parameter is doing what it should be doing and adjusting acfln before, so we get correct lengths */

    int tcor = acfln;

    stpln = nstps / nblcks;

    real ctmp;

    /* doing the if in the inner loop gives us for large files (50000 entries) about 2s slow-down */

    for ( t0=0; t0<nstps; t0 += stpln ) {
        a0 = data[t0];

        tmp = t0 + tcor;
        tt0max = min( nstps, tmp );

        ctmp = a0;

        for ( tt0=t0; tt0<tt0max; tt0++ ) {
            t = tt0 - t0;

            // if ( 1 & cntns )
            if ( cntns )
                ctmp = ctmp * cdata[tt0];
            else 
                ctmp = a0 * cdata[tt0];

            acf[t] = acf[t] + ctmp;
            // nrm[t] = nrm[t] + 1.0;

            /* can we do this with integers? what exactly will happen, when we convert floats to integers? */
            /* currently the below will fail, because not everything beyond break-point will be assigned correctly... */

            if ( ( ctmp < ctff ) && ( cntns ) ) {
                // int ttmp;
                // printf("hello\n");
                // for ( ttmp=tt0; tt0max; ttmp++ ) {
                //     t = ttmp - t0;
                //     nrm[t] += 1;
                // }
                break;
            }

        }

    }

    /*
     * real sqvar;
     * real tmpnrm;
     *
     * sqvar = sqrt ( var ) * sqrt( cvar );
     */

    /*
     * introduce individual normalization
     * but take care, because 0th entry could be zero (depending on what we're doing)
     */

    // nrm[0] = 1;

    for ( t=0; t<acfln; t++ ) {
        /* tmpnrm = nrm[t] * sqvar; */
        /* check here, integer division intented */
        /* other option is to generate norm externally */
        // nrm[t] += nblcks - ( t - 1 ) / stpln;
        // printf("%e\n", nrm[t]);
        acf[t] = acf[t] / nrm[t];
    }

}

void corfun_fft ( real * acf, int acfln, real * data, real * cdata, int nstps, int padd )
{
    int stpln;
    int t0, tt0, t;
    int tmp, i;
    int tt0max;
    int fftln;
    int datln;
    int crss = 0;

    fftw_plan plan_forward;
    fftw_plan plan_backward;
    fftw_complex * fwout;
    fftw_complex * crssfwout;
    fftw_complex * bwin;
    real * bwout;

    real a0;

    if ( data != cdata )
        crss = 1;

    if ( padd )
        datln = 2 * nstps;
    else
        datln = nstps;

    real tmpacf[datln];

    for ( i=0; i<datln; i++ )
        tmpacf[i] = ZERO;

    for ( i=0; i<acfln; i++ )
        acf[i] = ZERO;

    /* check here and maybe reduce it by an given input... */

    fftln = floor ( datln / 2 ) + 1;

    fwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );
    if ( crss )
        crssfwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );
    bwin = fftw_malloc ( sizeof ( fftw_complex ) * fftln );

    /* verify somehow that the complex part is always zeros */

    plan_forward = fftw_plan_dft_r2c_1d ( datln, data, fwout, FFTW_ESTIMATE );
    fftw_execute ( plan_forward );

    if ( crss ) {
        fftw_destroy_plan ( plan_forward );
        plan_forward = fftw_plan_dft_r2c_1d ( datln, cdata, crssfwout, FFTW_ESTIMATE );
        fftw_execute ( plan_forward );
    }

    /* produce square of absolute of complex numbers (square modulus) */
    /* find out why it does not matter that we don't scale it back before getting the square modulus */

    if ( crss )
        cmplx_conj_product ( bwin, fwout, crssfwout, fftln );
    else
        square_modulus ( bwin, fwout, fftln );

    plan_backward = fftw_plan_dft_c2r_1d ( datln, bwin, tmpacf, FFTW_ESTIMATE );

    fftw_execute ( plan_backward );

    /* scale data */
    /* normalize_fftw is double as expensive as the loop below, we need only half */

    // normalize_fftw ( tmpacf, datln );

    for ( i=0; i<acfln; i++ )
        tmpacf[i] /= datln;

    sum_acf ( acf, tmpacf, acfln );

    /* interestingly http://fy.chalmers.se/OLDUSERS/ahlstrom/fft_ac.pdf disagrees */
    if ( padd )
        normalize_acf_fft_padd ( acf, acfln );

    /* what if it's not padded? we should be fine, i think */

    fftw_destroy_plan ( plan_forward );
    fftw_destroy_plan ( plan_backward );

    fftw_free ( fwout );
    if ( crss )
        fftw_free ( crssfwout );
    fftw_free ( bwin );

    fftw_cleanup ();

}

/* read in data provided in file */
void read_data ( real * data, int dtrng, int nstps, char * txt, int offset )
{
    char rdln[MAXSTRLEN];
    char * vl;
    char * fname;

    FILE * fl;

    int i, j;

    fname = strtok ( txt, "\n");

    if ( !(( fl = fopen( txt, "r" ))) )
    {
        // printf("could not open '%s' file\n", txt);
        print_error ( FILE_NOT_FOUND, txt, __FILE__, __LINE__ );
    }

    for ( i=0; i<offset; i ++ ) {
        if ( fgets(rdln, MAXSTRLEN, fl) == NULL ) {
            // printf("Not enough data points in '%s'. Stopped at iteration %i\n", txt, i);
            print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );
        }
    }

    for ( i=0; i<nstps; i++ )
    {
        if ( fgets(rdln, MAXSTRLEN, fl) == NULL ) {
            // printf("Not enough data points in '%s'. Stopped at iteration %i\n", txt, i);
            print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );
        }

        vl = strtok (rdln, " \n");

        /* check here again, it should work, but somehow i am confused right now */
        /* maybe < would be correct, otherwise we are always skipping the first column */
        /* for ( j=0; j<dtrng; j++ ) `split string` */
        /* for ( j=1; j<=dtrng; j++ ) was here before */

        for ( j=0; j<dtrng; j++ )
            vl = strtok (NULL, " \n");

        if ( vl == NULL ) {
            // printf("Not enough columns in data file '%s'\n", txt);
            print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );
        }

        data[i] = atof ( vl );
    }

    fclose(fl);
}

void read_data_fxd_fmt ( real * data, int dtrng, int nstps, char * txt, int offset, int btln_ntr, int strd )
{
    char rdln[MAXSTRLEN];
    char vl[MAXSTRLEN];
    char * fname;
    char text[MAXSTRLEN];
    char * dummy;

    int btln_ln;
    // int btln_ntr;
    int ncls;

    FILE * fl;

    int i, j;

    fname = strtok ( txt, "\n");

    if ( !(( fl = fopen( txt, "r" ))) )
    {
        // printf("could not open '%s' file\n", txt);
        print_error ( FILE_NOT_FOUND, txt, __FILE__, __LINE__ );
    }

    rewind(fl);
    fgets ( text, MAXSTRLEN, fl );
    btln_ln = strlen(text);

    dummy = strtok ( text, " \n" );
    ncls = 1;

    while ( dummy != NULL ) {
        dummy = strtok ( NULL, " \n" );
        ncls += 1;
    }

    // this does not work, because we don't actually count the spaces...
    // if ( btln_ln % ncls )
    //     print_error ( FATAL, "Your file does not seem to be suited for fixed format access.\n", __FILE__, __LINE__ );
    // btln_ntr = btln_ln / ncls;

    /* check here, jump over first #offset lines */
    /* this is so useless, because below we are starting again from SEEK_SET and not SEEK_CUR */

    if ( fseek ( fl, offset*btln_ln , SEEK_SET ) )
        print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );

    for ( i=0; i<nstps; i += strd )
    {
        /* jump to specific line */

        /* this should fix the issue with SEEK_SET vs SEEK_CUR */
        if ( fseek ( fl, (offset+i)*btln_ln , SEEK_SET ) )
            print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );

        /* jump to specific position in line */

        if ( dtrng )
            if ( fseek ( fl, (dtrng)*btln_ntr , SEEK_CUR ) )
                print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );

        fread ( vl, btln_ntr , 1, fl );

        // printf("%s\n", vl);

        /* read next entry */

        data[i] = atof ( vl );
    }

    fclose(fl);
}

/* routine from allen/tildesley to calculate correlation function */

void read_input( int * nblcks, real * tstp, int * dtrng, int * nfls, int * nstps, int * padd, int * adjln, int * method, int * spec, int * read_acf, int * drvtv, int * crss, int * dtffst, real * thrmlz, int * cntns, int * slcd, real * ctff, int * fxdfmt, int * strd )
{
    FILE * datei;
    char * variable;
    char * value;
    char text[MAXSTRLEN];

    if ( !((datei = fopen("control", "r"))) )
    {
        // printf("could not open 'control' file\n");
        print_error ( FILE_NOT_FOUND, "control", __FILE__, __LINE__ );
    }

    while(fgets(text, MAXSTRLEN, datei) != NULL)
    {
        variable = strtok (text, " ");
        value = strtok (NULL, " ");

        if ( strstr(variable, "nblcks") != NULL )
        {
            *nblcks = atol(value);
        }
        else if ( strstr(variable, "tstp") != NULL )
        {
            *tstp = atof(value);
        }
        else if ( strstr(variable, "dtrng") != NULL )
        {
            *dtrng = atol(value);
        }
        else if ( strstr(variable, "nfls") != NULL )
        {
            *nfls = atol(value);
        }
        else if ( strstr(variable, "nstps") != NULL )
        {
            *nstps = atol(value);
        }
        else if ( strstr(variable, "padd") != NULL )
        {
            *padd = atol(value);
        }
        else if ( strstr(variable, "adjln") != NULL )
        {
            *adjln = atol(value);
        }
        else if ( strstr(variable, "method") != NULL )
        {
            if ( strstr ( value, "direct" ) != NULL )
                *method = DIRECT_ACF;
            else if ( strstr ( value, "fft" ) != NULL )
                *method = FFT_ACF;
            else if ( strstr ( value, "noacf" ) != NULL )
                *method = NO_ACF;
        }
        else if ( strstr(variable, "spec") != NULL )
        {
            *spec = atol(value);
        }
        else if ( strstr(variable, "read_acf") != NULL )
        {
            *read_acf = atol(value);
        }
        else if ( strstr(variable, "drvtv") != NULL )
        {
            *drvtv = atol(value);
        }
        else if ( strstr(variable, "crss") != NULL )
        {
            *crss = atol(value);
        }
        else if ( strstr(variable, "dtffst") != NULL )
        {
            *dtffst = atol(value);
        }
        else if ( strstr(variable, "thrmlz") != NULL )
        {
            *thrmlz = atof(value);
        }
        else if ( strstr(variable, "cntns") != NULL )
        {
            *cntns = atol(value);
        }
        else if ( strstr(variable, "slcd") != NULL )
        {
            *slcd = atol(value);
        }
        else if ( strstr(variable, "ctff") != NULL )
        {
            *ctff = atof(value);
        }
        else if ( strstr(variable, "fxdfmt") != NULL )
        {
            *fxdfmt = atol(value);
        }
        else if ( strstr(variable, "strd") != NULL )
        {
            *strd = atol(value);
        }
    }

    fclose(datei);

}

void sum_acf ( real * acf, real * tmpacf, int len)
{
    int i;

    for ( i=0; i<len; i++ )
        acf[i] += tmpacf[i];

    return;
}

void output_acf ( real * acf, int acfln, real tstp )
{
    FILE * fout;
    int i;

    if ( !((fout = fopen ( "acf.dat", "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "acf.dat", __FILE__, __LINE__ );
    }

    for ( i=0; i<acfln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", i*tstp, acf[i]);

    fclose(fout);
}

void normalize_acf ( real * acf, int acfln )
{
    int i;
    real a0;

    a0 = acf[0];

    /* get proper normalization with respect to average and stddev, see TODO file*/
    for ( i=0; i<acfln; i++ )
        acf[i] /= a0;

}

void normalize_acf_nfls ( real * acf, int len, int nfls )
{
    int i;
    real nrm;

    nrm = ( real ) nfls;

    /* get proper normalization with respect to average and stddev, see TODO file*/
    for ( i=0; i<len; i++ )
        acf[i] /= nrm;

}

void square_modulus ( fftw_complex * out, fftw_complex * in, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i][0] = sqr ( in[i][0] ) + sqr ( in[i][1] );
        out[i][1] = ZERO;
    }

}

void square_modulus_real ( real * out, fftw_complex * in, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i] = sqr ( in[i][0] ) + sqr ( in[i][1] );
    }

}

void cmplx_conj_product ( fftw_complex * out, fftw_complex * in1, fftw_complex * in2, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i][0] = in1[i][0] * in2[i][0] + in1[i][1] * in2[i][1];
        out[i][1] = in1[i][0] * in2[i][1] - in2[i][0] * in1[i][1];
        // printf("%f\n", out[i][1]);
    }

}

void normalize_fftw ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] /= len;

}

void collect_norm ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] += 1.;

}

void normalize_acf_fft_padd ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] /= ( len - i );

}

void sum_fft ( fftw_complex * fft, fftw_complex * tmpfft, int len)
{
    int i;

    for ( i=0; i<len; i++ ) {
        fft[i][0] += tmpfft[i][0];
        fft[i][1] += tmpfft[i][1];
    }

    return;
}

void get_spectrum ( fftw_complex * mpltd, real * acf, int acfln )
{
    int i;
    int fftln;

    fftw_complex * fwout;
    fftw_plan plan_forward;
    
    fftln = floor ( acfln / 2 ) + 1;
    // fwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );

    plan_forward = fftw_plan_dft_r2c_1d ( acfln, acf, mpltd, FFTW_ESTIMATE );

    fftw_execute ( plan_forward );

    // for ( i=0; i<fftln; i++ ) {
    //     mpltd[i] = fwout[i][0];
    // }

    fftw_destroy_plan ( plan_forward );
    // fftw_free ( fwout );
    fftw_cleanup ();

}

void output_spectrum ( fftw_complex * mpltd, int spcln, real dt, real thrmlz )
{
    int i;
    FILE * fout;

    real fs;
    real df;

    fs = .5 / dt;
    df = fs / spcln;
    
    if ( !((fout = fopen ( "spectrum.dat", "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "spectrum.dat", __FILE__, __LINE__ );
    }

    if ( thrmlz > 0.0 ) {
        real wvn2frq;

        wvn2frq = SOL * 100.;

        for ( i=0; i<spcln; i++ )
            mpltd[i][0] *= ( 1. - exp ( -1. / KBOLTZ / thrmlz * i * df * PLANCK ) );
    }

    for ( i=0; i<spcln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", ( real ) i * df / SOL / 100. , mpltd[i][0]);
        // fprintf(fout, "%20.10e\n", mpltd[i]);

    fclose(fout);
}

void output_spectrum_real ( real * mpltd, int spcln, real dt, real thrmlz )
{
    int i;
    FILE * fout;

    real fs;
    real df;

    fs = .5 / dt;
    df = fs / spcln;
    
    if ( !((fout = fopen ( "spectrum.dat", "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "spectrum.dat", __FILE__, __LINE__ );
    }

    if ( thrmlz > 0. ) {
        real wvn2frq;

        wvn2frq = SOL * 100.;

        for ( i=0; i<spcln; i++ )
            mpltd[i] *= ( 1. - exp ( -1. / KBOLTZ / thrmlz * i * df * PLANCK ) );
    }

    for ( i=0; i<spcln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", ( real ) i * df / SOL / 100. , mpltd[i]);
        // fprintf(fout, "%20.10e\n", mpltd[i]);

    fclose(fout);
}

void drvtv_fv_pnt ( real * drv, real * data, int len, real h )
{
    int i;
    int fvp;

    real dt;

    /* check here if we need to do something about the units */
    dt = h / 1.E-15;

    fvp = len - 2;

    for ( i=2; i<fvp; i++ ) {
        drv[i] = -1. * data[i+2] + 8. * data[i+1] - 8. * data[i-1] + data[i-2];
        drv[i] /= ( 12. * dt );
        // drv[i] /= 12.;
    }

    drv[1] = data[2] - data[0];
    drv[1] /= ( 2. * dt );
    // drv[1] /= 2.;

    drv[len-2] = data[len-1] - data[len-3];
    drv[len-2] /= ( 2. * dt );
    // drv[len-2] /= 2.;

    drv[0] = data[1] - data[0];
    drv[0] /=  dt;
    // drv[0] /= 1;

    drv[len-1] = data[len-1] - data[len-2];
    drv[len-1] /= dt;
    // drv[len-1] /= 1;

}

void blocked_squared_spectrum ( real * spcout, int acfln, int nblcks, real * data, int datln )
{
    int i;
    int cntfft = 0;
    int blckln;
    int fft_blckln;
    int tmpln;

    real tmpout[acfln];
    fftw_complex * fwout;
    fftw_plan plan_forward;

    blckln = floor ( datln / nblcks );
    fft_blckln = floor ( blckln / 2 ) + 1;

    fwout = fftw_malloc ( sizeof ( fftw_complex ) * fft_blckln );

    /* check here and add individual padding? (dunt make no sense, IMHO) */
    for ( i=0; i<acfln; i++ )
        tmpout[i] = ZERO;

    for ( i=0; i<datln; i += blckln ) {
        tmpln = datln - i;

        if ( tmpln < blckln )
            break;

        plan_forward = fftw_plan_dft_r2c_1d ( blckln, &(data[i]), fwout, FFTW_ESTIMATE );

        fftw_execute ( plan_forward );

        square_modulus_real ( tmpout, fwout, acfln );

        sum_acf ( spcout, tmpout, acfln );

        fftw_destroy_plan ( plan_forward);

        cntfft++;

    }

    fftw_free ( fwout );
    fftw_cleanup ();

    for ( i=0; i<acfln; i++ )
        spcout[i] /= ( real ) cntfft;

}

real get_mean ( real * data, int len )
{
    int i;

    real mean = ZERO;

    for ( i=0; i<len; i++ )
        mean += data[i];

    mean /= ( real ) len;

    return mean;
}

void subtract_value ( real * data, int len, real val )
{
    int i; 

    for ( i=0; i<len; i++ )
        data[i] -= val;
}

real get_variance ( real * data, int len )
{
    int i;
    real msq = ZERO;
    real var;
    real mean;

    mean = get_mean ( data, len );

    for ( i=0; i<len; i++ )
        msq += sqr ( data[i] );

    msq /= ( real ) len;

    var = msq - sqr ( mean );

    return var;
}

real get_variance_with_mean ( real * data, int len, real mean )
{
    int i;
    real msq = ZERO;
    real var;

    for ( i=0; i<len; i++ )
        msq += sqr ( data[i] );

    msq /= ( real ) len;

    var = msq - sqr ( mean );

    return var;
}

void generate_nrm ( real * nrm, int nrmln, int nstps, int nblcks, int acfln )
{

    int i;
    int stpln, t, t0, tt0, tcor, tmp, tt0max;

    stpln = nstps / nblcks;
    tcor = acfln;

    /* check here if acfln is the right ending for the norm */
    /* and also confirm that norm is generated correctly, although I think I did that */

    for ( i=0; i<acfln; i++ )
        nrm[i] = ZERO;

    for ( t0=0; t0<nstps; t0 += stpln ) {
        tmp = t0 + tcor;
        tt0max = min( nstps, tmp );

        for ( tt0=t0; tt0<tt0max; tt0++ ) {
            t = tt0 - t0;
            nrm[t] = nrm[t] + 1.0;
        }
    }

}

void check_malformed_input ( int nblcks, real tstp, int dtrng, int nfls, int nstps, int padd, int adjln, int method, int spec, int read_acf, int drvtv, int crss, int dtffst, real thrmlz, int cntns, int slcd, real ctff, int fxdfmt, int strd )
{

    if ( ( slcd ) && ( !( adjln ) ) )
    {
        print_error(MISSING_INPUT_PARAM, "adjln", __FILE__, __LINE__);
    }

    if ( ( slcd ) && ( method == FFT_ACF ) )
    {
        print_error(NOT_IMPLEMENTED, "Sliced input plus FFT method", __FILE__, __LINE__);
    }

    if ( ( slcd ) && ( method == NO_ACF ) )
    {
        print_error(NOT_IMPLEMENTED, "Sliced input plus NO_ACF method", __FILE__, __LINE__);
    }

    if ( ( cntns ) && ( method == FFT_ACF ) )
    {
        print_error(NOT_IMPLEMENTED, "Continuous correlation function with FFT", __FILE__, __LINE__);
    }

    if ( ( cntns ) && ( method == NO_ACF ) )
    {
        print_error( NONSENSICAL, "Calculating continuous correlation function without calculating correlation function", __FILE__, __LINE__);
    }

    if ( ( padd ) && ( method == DIRECT_ACF ) )
    {
        print_warning ( MEMORY_WASTE, "padding is not used in DIRECT calculation" );
    }

    if ( ! ( padd ) && ( method == FFT_ACF ) )
    {
        print_warning ( YOU_KNOW_WHAT, "Input padding is generally recommended with FFT method, but you probably know what you're doing" );
    }

    if ( ( adjln ) && ( method == FFT_ACF ) )
    {
        // check here, maybe a NOT_IMPLEMENTED error would be better
        print_warning ( NO_EFFECT, "Adjusting lengths while striding over data with FFT data" );
    }

    if ( !( nstps ) )
    {
        print_error ( MISSING_INPUT_PARAM, "nstps" , __FILE__, __LINE__);
    }
            
    if ( !( nfls ) )
    {
        print_error ( MISSING_INPUT_PARAM, "nfls", __FILE__, __LINE__ );
    }

    if ( (cntns) && ( ctff == -10000. ) )
    {
        print_warning ( YOU_KNOW_WHAT, "Providing a cutoff for continuous correlation function can significantly speed up things\n" );
    }

    if ( ( !(cntns) ) && ( ctff != -10000. ) )
    {
        print_warning ( YOU_KNOW_WHAT, "'ctff' has only an effect on continuous correlation functions\n" );
    }
    
    if ( ( strd != 1 ) && ( !(fxdfmt) ) )
    {
        print_error ( NOT_IMPLEMENTED, "Reading data in strides > 1 for non-fixed format", __FILE__, __LINE__ );
    }
            
}
