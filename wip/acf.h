/*

SPEAR is a C program to calculate correlation functions and spectra
Copyright 2013 Frank Uhlig (uhlig.frank@gmail.com)

This file is part of SPEAR.

SPEAR is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPEAR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPEAR.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <fftw3.h>

typedef enum methods_e
{
    DIRECT_ACF=1,
    FFT_ACF,
    NO_ACF,
} methods_t;

// typedef float real;
typedef double real;
void read_data (real * data, int dtrng, int nstps, char * txt, int offset );
void read_data_fxd_fmt (real * data, int dtrng, int nstps, char * txt, int offset, int btln_ntr, int strd );
void read_input(int * nblcks, real * tstp, int * dtrng, int * nfls, int * nstps, int * padd, int * adjln, int * method, int * spec, int * read_acf, int * drvtv, int * crss, int * dtffst, real * thrmlz, int * cntns, int * slcd, real * ctff, int * fxdfmt, int * strd );
void corfun_direct ( real * acf, int acfln, real * data, real * cdata, int nstps, int nblcks, int cntns, real * nrm, real ctff );
void corfun_fft (real * acf, int acfln, real * data, real * cdata, int nstps, int padd );
void sum_acf ( real * acf, real * tmpacf, int len);
void output_acf ( real * acf, int acfln, real tstp );
void normalize_acf ( real * acf, int acfln );
void normalize_acf_nfls ( real * acf, int len, int nfls );
void copy_data ( real * out, real * in, int start, int len );
void square_modulus ( fftw_complex * out, fftw_complex * in, int len );
void square_modulus_real ( real * out, fftw_complex * in, int len );
void cmplx_conj_product ( fftw_complex * out, fftw_complex * in1, fftw_complex * in2, int len );
void normalize_fftw ( real * data, int len );
void collect_norm ( real * data, int len );
void normalize_acf_fft_padd ( real * data, int len );
void sum_fft ( fftw_complex * fft, fftw_complex * tmpfft, int len);
void print_fingers ();
void print_feet ( int lr );
void get_spectrum ( fftw_complex * mpltd, real * acf, int acfln );
void output_spectrum ( fftw_complex * mpltd, int spcln, real dt, real thrmlz );
void output_spectrum_real ( real * mpltd, int spcln, real dt, real thrmlz );
void drvtv_fv_pnt ( real * drv, real * data, int len, real h );
void blocked_squared_spectrum ( real * spcout, int acfln, int nblcks, real * data, int datln );
real get_mean ( real * data, int len );
void subtract_value ( real * data, int len, real val );
real get_variance ( real * data, int len );
real get_variance_with_mean ( real * data, int len, real mean );
void generate_nrm ( real * nrm, int nrmln, int nstps, int nblcks, int acfln );
void check_malformed_input ( int nblcks, real tstp, int dtrng, int nfls, int nstps, int padd, int adjln, int method, int spec, int read_acf, int drvtv, int crss, int dtffst, real thrmlz, int cntns, int slcd, real ctff, int fxdfmt, int strd );
