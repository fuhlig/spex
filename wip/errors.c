/*

SPEAR is a C program to calculate correlation functions and spectra
Copyright 2013 Frank Uhlig (uhlig.frank@gmail.com)

This file is part of SPEAR.

SPEAR is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPEAR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPEAR.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "errors.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_error(int errno, char * detail, char * file, int line)
{

    print_error_header();
    
    switch ( errno )
    {
        case FILE_NOT_FOUND:
            printf("File '%s' could not be opened\n", detail);
            break;
        case MISSING_INPUT_PARAM:
            printf("Task cannot be completed without specifying input parameter: '%s'\n", detail);
            break;
        case NOT_IMPLEMENTED:
            printf("%s not implemented!\n", detail);
            break;
        case NONSENSICAL:
            printf("%s is non-sensical!\n", detail);
            break;
        case INCOMPLETE_INPUT:
            printf("%s\n", detail);
            break;
        case NOT_ENOUGH_DATA:
            printf("Not enough data in '%s'\n", detail);
            break;
        case FATAL:
            printf("Fatal error: %s\n", detail);
            break;
        case UNASSIGNED_ERROR:
        default:
            printf("An error occurred that has not yet been assigned to a category!\n");
            break;
    }

    printf( "Stopping in file %s at line %d\n", file, line );
    
    print_error_footer();
    
    exit ( errno );
}

void print_warning(int warno, char * detail)
{

    print_warning_header();
    
    switch ( warno )
    {
        case MEMORY_WASTE:
            printf("We are wasting memory, because %s\n", detail);
            break;
        case YOU_KNOW_WHAT:
            printf("%s\n", detail);
            break;
        case NO_EFFECT:
            printf("%s has no effect.\n", detail);
            break;
        case UNASSIGNED_WARNING:
        default:
            printf("You are doing something possibly dangerous, but no warning has been assigned yet. Sorry!\n");
            break;
            
    }
    
    print_warning_footer();

}

void print_error_header()
{
    printf("\n!!!!!!!!! E R R O R !!!!!!!!!\n\n");
}

void print_error_footer()
{
    printf("\n!!!!!!!!! E R R O R !!!!!!!!!\n\n");
}

void print_warning_header()
{
    printf("\n!!!!!!!!! W A R N I N G !!!!!!!!!\n\n");
}

void print_warning_footer()
{
    printf("\n!!!!!!!!! W A R N I N G !!!!!!!!!\n\n");
}
