/*

SPEX is a C program to calculate correlation functions and spectra
Copyright 2013 Frank Uhlig (uhlig.frank@gmail.com)

This file is part of SPEX.

SPEX is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPEX is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPEX.  If not, see <http://www.gnu.org/licenses/>.

*/

/* do we need to normalize each individual correlation function to <a0,a0> before summing them or just afterwards? */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <fftw3.h>
#include "errors.h"
#include "fingers.h"
#include "acf.h"
#include "constants.h"

#define sqr(x) ((x)*(x))
#define MAXSTRLEN 500000
#define SOL 299792458
#define KBOLTZ 1.3806488E-23
#define PLANCK 6.62606957E-34
#define ZERO 0.

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
  ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

int main (int argc, char *argv[])
{
    /* input options */
    int i, j, k;
    int nblcks = 1;
    int dtrng = 1;
    int padd = 0;
    int adjln = 0;
    int nfls = 0;
    int nstps = 0;
    int method = DIRECT_ACF;
    int spec = 0;
    int read_acf = 0;
    int drvtv = 0;
    int crss = 0;
    int dtffst = 0;
    int cntns = 0;
    int slcd = 0;
    int sclng = 0;
    int nchnk = 1;
    /* check here, and maybe check if we want this to be the default */
    int nrmlzd = 1;
    int ndvdlnrm = 0;
    int prntll = 0;
    int window = 0;
    int strd = 1;
    int raman = 0;
    int spcknd = GENERIC;
    int fxdt = 0;
    /* check here, for some reason datln was a real before */
    int datln;
    real thrmlz = -1.;
    real tstp = .5E-15;
    real datscl = 1.;
    real crsscl= 1.;
    /* check here and find a better way of doing it */
    real ctff = -10000.;

    real * acf;
    real * tmpacf;
    real * nrm;
    int blckln;
    int acfln;
    int done = 0;
    int lr = 0;
    int ppf = 0;
    int pps = 0;
    int tstart;
    int tstop;
    int fxdfmt = 0;
    int statnrm = 0;
    int ncmpnt = 1;
    int mltpl_cl;

    char txt[MAXSTRLEN];
    char * dummy;

    FILE * fls;
    FILE * fl;

    tstart = clock ();

    if ( ( argc > 1 ) && ( strstr(argv[1], "are you ready?") != NULL ) ) {
        print_moving_fingers ();
        ppf = 1;
    }
    else if ( ( argc > 1 ) && ( strstr(argv[1], "-v") != NULL ) ) {
        ppf = 1;
    }

    if ( !((fls = fopen("files", "r"))) )
    {
        print_error ( FILE_NOT_FOUND, "files", __FILE__, __LINE__);
    }

    read_input ( &nblcks, &tstp, &dtrng, &nfls, &nstps, &padd, &adjln, &method, &spec, &read_acf, &drvtv, &crss, &dtffst, &thrmlz, &cntns, &slcd, &ctff, &fxdfmt, &sclng, &nrmlzd, &ndvdlnrm, &prntll, &window, &strd, &statnrm, &spcknd, &fxdt, &nchnk );

    necessary_input_manipulations ( &spcknd, &dtrng, &ncmpnt );

    check_malformed_input ( nblcks, tstp, dtrng, nfls, nstps, padd, adjln, method, spec, read_acf, drvtv, crss, dtffst, thrmlz, cntns, slcd, ctff, fxdfmt, sclng, nrmlzd, ndvdlnrm, prntll, window, statnrm, spcknd, nchnk );

    mltpl_cl = dtrng - 1;

    nstps /= strd;
    tstp *= strd;
    nblcks /= strd;

    if ( cntns )
        print_warning ( YOU_KNOW_WHAT, "We are calculating a continuous correlation function. Be careful!!! and check code for details :)" );

#ifdef EXPERIMENTAL
    print_warning ( EXPWARNING, "" );
#endif

    /* just a fix so that we don't need to check conditions below for the progress counter */
    if ( ( ppf ) && ( nfls == 1 ) ) {
        ppf = 0;
        pps = 1;
    }

    real ** data;
    real ** cdata;

    // if ( padd )
    //     datln = 2 * nstps;
    // else
    //     datln = nstps;

    datln = nstps;

    nstps /= nchnk;

    // FUDO| substitute by 2D and 1D allocation functions
    // FUDO| for now, we'll use the calloc function, to avoid having it to zero out, is that needed at all?

    data = ( real ** ) malloc ( dtrng * sizeof ( real * ) );
    for ( i=0; i<dtrng; i++ )
        data[i] = ( real * ) calloc ( datln, sizeof ( real ) );
        // data[i] = ( real * ) malloc ( datln * sizeof ( real ) );

    if ( ( crss ) || ( spcknd == RAMAN_ANI ) || ( spcknd == RAMAN_SAN ) ) {
        cdata = ( real ** ) malloc ( dtrng * sizeof ( real * ) );
        for ( i=0; i<dtrng; i++ )
            cdata[i] = ( real * ) calloc ( datln, sizeof ( real ) );
            // cdata[i] = ( real * ) malloc ( datln * sizeof ( real ) );
    }

    /*
    * for ( i=0; i<datln ; i++ )
    *     data[i] = ZERO;

    * if ( crss )
    *     for ( i=0; i<datln ; i++ )
    *         cdata[i] = ZERO;
    */

    // initialize empty acf and nrm data arrays of proper length

    blckln = nstps / nblcks;

    switch ( method )
    {
        case DIRECT_ACF:
            if ( adjln )
                acfln = nstps;
            else
                acfln = blckln;
            break;
        case FFT_ACF:
            /* if padded, we'll throw away the second half of the correlation function */
            acfln = nstps;
            break;
        case NO_ACF:
            // if ( padd )
            //     acfln = 2 * nstps;
            // else
            // check here, below, because later we fft, we only need this size
            acfln = floor ( blckln / 2 ) + 1;
            break;
        default:
            acfln = nstps;
            break;
    }

    acf = ( real * ) malloc ( acfln * sizeof ( real ) );
    tmpacf = ( real * ) malloc ( acfln * sizeof ( real ) );
    nrm = ( real * ) malloc ( acfln * sizeof ( real ) );

    for ( i=0; i<acfln; i++ ) {
        acf[i] = ZERO;
        nrm[i] = ZERO;
        tmpacf[i] = ZERO;
    }

    printf("dtrng: %i, nfls: %i, nblcks: %i\n", dtrng, nfls, nblcks);

    if ( ( method == DIRECT_ACF ) && ( !( slcd ) ) )
        generate_nrm ( nrm, acfln, nstps, nblcks, acfln );

    //FUOD| check here, the new read_data function probably breaks the read_acf part and we should check that, later
    if ( read_acf ) {
        char acfnm[8] = "acf.dat";

        if ( method == NO_ACF ) {
            free ( tmpacf );
            tmpacf = ( real * ) malloc ( nstps * sizeof ( real ) );
            read_data ( &tmpacf , 1, datln, &acfnm[0], dtffst, strd, raman );
        }
        else {
            read_data ( &acf , 1, datln, &acfnm[0], dtffst, strd, raman );
        }
    }
    else {
        for ( i=0; i<nfls; i++ )
        {
            if ( fgets(txt, MAXSTRLEN, fls) == NULL ) {
                print_error ( NOT_ENOUGH_DATA, "files", __FILE__, __LINE__ );
                // printf("Not enough entries in 'files'. Stopped at iteration %i.\n", i);
            }

            char * fname;
            char * crss_fname;
            // char slcdmm[MAXSTRLEN];

            // strncpy ( slcdmm, txt, MAXSTRLEN );
            // printf("%s\n", txt);

            fname = strtok ( txt, " \n" );

#ifdef EXPERIMENTAL
            if ( sclng )
                datscl = atof ( strtok ( NULL, " \n" ) );
#endif

            if ( crss ) {
                crss_fname = strtok ( NULL, " \n" );

#ifdef EXPERIMENTAL
                if ( sclng )
                    crsscl = atof ( strtok ( NULL, " \n" ) );
#endif
            }

            int tmpacfln;

            if ( slcd )
            {
                char * start, *stop;
                int tmp;

                start = strtok ( NULL, " \n" );
                if ( start == NULL )
                    print_error ( INCOMPLETE_INPUT, "Not enough slice entries in files" , __FILE__, __LINE__);

                stop = strtok ( NULL, " \n" );
                if ( stop == NULL )
                    print_error ( INCOMPLETE_INPUT, "Not enough slice entries in files", __FILE__, __LINE__);

                dtffst = atol(start);
                tmp = atol(stop);

                //FUDO| check here, nstps vs datln
                nstps = tmp - dtffst;

                nblcks = nstps;
                tmpacfln = nstps;

                generate_nrm ( nrm, tmpacfln, nstps, nblcks, tmpacfln );
                // FUDO|

                // printf("file: %s start: %i stop: %i\n", fname, dtffst, tmp);

            }
            else
            {
                tmpacfln = acfln;
            }

            // FU| here, load all data at once into two-dimensional array
            // FU| for RAMAN, we need to prep the data, either:
            // FU|      calculate isotropic polarizability
            // FU|      calculate anisotropic polarizability
            // FU| resort the data (and de-allocate parts of it) to represent only the parts that need to be correlated

            if ( fxdfmt )
                print_error ( NOT_IMPLEMENTED, "Fixed format reading", __FILE__, __LINE__ );

            // printf("%i %i %s %i %i %i\n", dtrng, nstps, fname, dtffst, strd, mltpl_cl);
            read_data ( data, dtrng, datln, fname, dtffst, strd, mltpl_cl );

            if ( crss )
                read_data ( cdata, dtrng, datln, crss_fname, dtffst, strd, mltpl_cl );
            else if ( ( spcknd == RAMAN_ANI ) || ( spcknd == RAMAN_SAN ) )
                // FUDO| change this to re-assigning the data, which should be much faster than re-reading everything
                read_data ( cdata, dtrng, datln, fname, dtffst, strd, mltpl_cl );

            int tcr = 0;

            // FUREM| remove this shit
            // printf("%f %f\n", data[0][0], cdata[0][0]);
            // printf("%f %f\n", data[0][5000], cdata[0][5000]);

            if ( ( crss ) || ( spcknd == RAMAN_ANI ) )
                tcr = 1;

            // for ( j=0; j<ncmpnt; j++ )
            // {

            //     if ( fxdt ) {
            //         int nfx;
            //         char temp[80];
            //         sprintf( temp, "%s_%i_%i", "fixed", i, j);
            //         nfx = fix_data ( data[j], datln );
            //         output_data_titled ( data[j], datln, tstp, temp );
            //         if ( nfx )
            //             printf("Fixed %i entries in file %s, column %i\n", nfx, fname, j);

            //         if ( tcr ) {
            //             sprintf( temp, "%s_%i_%i", "cdata_fixed", i, j);
            //             nfx = fix_data ( cdata[j], datln );
            //             output_data_titled ( cdata[j], datln, tstp, temp );
            //             if ( nfx )
            //                 printf("Fixed %i entries in file %s, column %i\n", nfx, fname, j);
            //         }
            //     }
            // }

            prep_data_for_correlation ( data, cdata, datln, spcknd, crss );

            for ( j=0; j<ncmpnt; j++ )
            {

                if ( fxdt ) {
                    int nfx;
                    char temp[80];
                    sprintf( temp, "%s_%i_%i", "fixed", i, j);
                    nfx = fix_data ( data[j], datln );
                    output_data_titled ( data[j], datln, tstp, temp );
                    if ( nfx )
                        printf("Fixed %i entries in file %s, column %i\n", nfx, fname, j);

                    if ( tcr ) {
                        sprintf( temp, "%s_%i_%i", "cdata_fixed", i, j);
                        nfx = fix_data ( cdata[j], datln );
                        output_data_titled ( cdata[j], datln, tstp, temp );
                        if ( nfx )
                            printf("Fixed %i entries in file %s, column %i\n", nfx, fname, j);
                    }
                }

                // printf("%i\n", j );

                // printf("%i\n", j);
                // printf("%s\n", fname);
                // printf("%s\n", crss_fname);

#ifdef EXPERIMENTAL
                if ( sclng )
                    scale_data ( data[j], datln, datscl );
#endif

#ifdef EXPERIMENTAL
                if ( tcr ) {
                    if ( sclng )
                        scale_data ( cdata[j], datln, crsscl );
                }
#endif


                // printf("%f\n", cdata[0]);
                // printf("\n\n");

                if ( drvtv ) {
                    if ( nstps <= 1 )
                        print_error ( FATAL, "Cannot calculate derivative with only one point. Please check your input!", __FILE__, __LINE__);

                    real * drv = ( real * ) malloc ( nstps * sizeof ( real ) );
                    drvtv_fv_pnt ( drv, data[j], nstps, tstp );

                    for ( k=0; k<nstps; k++ ) {
                        data[j][k] = drv[k];
                    }

                    if ( tcr ) {
                        drvtv_fv_pnt ( drv, cdata[j], nstps, tstp );

                        for ( k=0; k<nstps; k++ ) {
                            cdata[j][k] = drv[k];
                        }
                    }

                    free ( drv );
                }

                switch ( method )
                {
                    case DIRECT_ACF:
                        if ( tcr )
                            corfun_direct ( tmpacf, tmpacfln, data[j], cdata[j], nstps, nblcks, cntns, nrm, ctff, statnrm );
                        else
                            corfun_direct ( tmpacf, tmpacfln, data[j], data[j], nstps, nblcks, cntns, nrm, ctff, statnrm );
                        break;
                    case FFT_ACF:
                        if ( tcr )
                            corfun_fft ( tmpacf, tmpacfln, data[j], cdata[j], datln, padd, statnrm, nchnk );
                        else
                            corfun_fft ( tmpacf, tmpacfln, data[j], data[j], datln, padd, statnrm, nchnk );
                        break;
                    case NO_ACF:
                        // check here, we senselessly copy all data, also the zeros, that are already there, considerable overhead for many files
                        // check here, also, if it is okay to average the data before doing the actual fft, or do we need to do the FFTs before and average then

                        // in principle we can also cheat our way here, to get blocked spectrum out of ACF if readacf is != 0

                        blocked_squared_spectrum ( tmpacf, tmpacfln, nblcks, data[j], nstps );
                        /* currently not available with crss option, because it is not clear to me how exactly to do it */
                        if ( tcr ) {
                            // printf("Cross-correlation and 'noacf' method not implemented, yet\n");
                            print_error ( NOT_IMPLEMENTED, "Cross-correlation and 'NOACF' method", __FILE__, __LINE__ );
                        }

                        break;
                    default:
                        break;
                }

                if ( ndvdlnrm )
                    normalize_acf ( tmpacf, tmpacfln );

                if ( prntll )
                    /* check here, if the indexing is correct */
                    output_acf_numbered ( tmpacf, tmpacfln, tstp, i * dtrng + j );

                /* check here, and make if clause for printing of individual correlation function */

                if ( pps ) {
                    printf("done: %6.2f\r", ( real ) ( j + 1 ) / dtrng );
                    fflush(stdout);
                }

                // check here, what happens if we do _not_ discard the second half of the spectrum, above change acfln = nstps to acfln = 2 * nstps if ( padd )
                sum_acf ( acf, tmpacf, tmpacfln );
            }

            // done = i / nfls * 100;
            // if ( done % 10 == 0 ) {
            //     print_feet ( lr );
            //     lr++;
            // }

            if ( ppf ) {
                printf("done: %6.2f\r", ( real ) ( i + 1 ) / nfls);
                fflush(stdout);
            }
        }

        printf("done: %6.2f\n", ( real ) i / nfls);

        /* check normalization again, again and again */

        int cnt = dtrng * nfls;

        if ( ( method == FFT_ACF ) || ( method == DIRECT_ACF ) ) {

            /* check here, is this really needed? or rather, in which cases is it need?
             * if we have the response of three components of a dipole, we should probably average
             * over all components
             * if we have contributions from single molecules, then the total signal will be about
             * the sum of all individual contributions as it is a system property and dipole moment
             * is an extensive property - BAM!
             *
             * for ( i=0; i<acfln; i++ )
             *     acf[i] /= ( real ) cnt;
             */

            /* output not normalized acf as well and decide which one we want to use */
            if ( nrmlzd )
                normalize_acf ( acf, acfln );

            // normalize_acf_nfls ( acf, acfln, nfls*dtrng );
            output_acf ( acf, acfln, tstp );
        }
        else if ( method == NO_ACF ) {
            for ( i=0; i<acfln; i++ )
                acf[i] /= ( real ) cnt;
        }

    }

    if ( spec ) {
        fftw_complex * mpltd;

        int spcln;
        int tmpacfln;

        if ( method == NO_ACF ) {
            if ( read_acf )
                blocked_squared_spectrum ( acf, acfln, nblcks, tmpacf, nstps );

            output_spectrum_real ( acf, acfln, tstp, thrmlz );
        }
        else {
            if ( spec == 1 ) {
                spcln = floor ( acfln / 2 ) + 1;
                tmpacfln = acfln;
            }
            else {
                spcln = floor ( spec / 2 ) + 1;
                tmpacfln = spec;
            }

            // mpltd = ( real * ) malloc ( spcln * sizeof ( real ) );
            mpltd = fftw_malloc ( sizeof ( fftw_complex ) * spcln );

            /* FUDO| could this potentially break stuff if shit is averaged over many acfs? */
            /* FUDO| it just does not work like this with isotropic Raman part (because doesn't decay to zero) */
            /* FUDO| for this we need to modify the applied window function */

            if ( window ) {
                apply_window_function ( acf, window, acf, acfln );
                output_acf_titled ( acf, acfln, tstp, "windowed" );
            }

            get_spectrum ( mpltd, acf, tmpacfln );

            output_spectrum ( mpltd, spcln, tstp, thrmlz );

            fftw_free(mpltd);
        }

    }

    for ( i=0; i<dtrng; i++ )
        free ( data[i] );
    free ( data );

    if ( ( crss ) || ( spcknd == RAMAN_ANI ) ) {
        for ( i=0; i<dtrng; i++ )
            free ( cdata[i] );
        free ( cdata );
    }
    free(acf);
    free(tmpacf);
    free(nrm);

    fclose(fls);

    tstop = clock ();

    real dt = ( real ) ( tstop - tstart ) / CLOCKS_PER_SEC;
    printf("It took me %5.2f s to do all the work for you!\n", dt );

    if ( dt > 60. )
        printf("That took long. What the hell are you doing?\n");
}

void corfun_direct ( real * acf, int acfln, real * data, real * cdata, int nstps, int nblcks, int cntns, real * nrm, real ctff, int statnrm )
{
    /* this is a basic copy of what is given in Allen/Tildesley */

    int crss = 0;
    int stpln;
    int t0, tt0, t;
    int tmp, i;
    int tt0max;

    real a0;
    real mean = 0.;
    real cmean = 0.;
    real var = 1.;
    real cvar = 1.;

    // real nrm[acfln];

    for ( i=0; i<acfln; i++ ) {
        // nrm[i] = ZERO;
        acf[i] = ZERO;
    }

    /* check here, this is strictly speaking only correct if we use the data for only one correlation function */
    /* otherwise we need to recalculate the mean and variance independently for each segment of the data */
    /* holds for second-order stationary processes, better check with augmented Dickey-Fuller test (http://en.wikipedia.org/wiki/Dickey%E2%80%93Fuller_test) */
    /* if second-order startionary process then mean and variance are time-independent */
    /* on short time scales as in AIMD it could actually matter... */

    /* in general it might just be wrong to apply this statistically correct form of the autocorrelation function */
    /* because it is not necessarily what would be measured.... or is it? */
    /* it will stay commented until we know exactly how to deal with it */

    if ( statnrm ) {
        mean = get_mean ( data, nstps );
        var = get_variance_with_mean ( data, nstps, mean );

        // FUCHECK| we could in principle leave this in, but it will alter the state of our data, why for now we include it at the bottom right now
        // subtract_value ( data, nstps, mean );

        if ( data != cdata ) {
            crss = 1;

            cmean = get_mean ( cdata, nstps );
            cvar = get_variance_with_mean ( cdata, nstps, cmean );

            // subtract_value ( cdata, nstps, cmean );

        }
        else {
            cmean = mean;
            cvar = var;
        }
    }

    /* check again if adjln parameter is doing what it should be doing and adjusting acfln before, so we get correct lengths */

    int tcor = acfln;

    stpln = nstps / nblcks;

    real ctmp;

    /* doing the if in the inner loop gives us for large files (50000 entries) about 2s slow-down */

    for ( t0=0; t0<nstps; t0 += stpln ) {
        a0 = data[t0];

        tmp = t0 + tcor;
        tt0max = min( nstps, tmp );

        ctmp = a0;

        for ( tt0=t0; tt0<tt0max; tt0++ ) {
            t = tt0 - t0;

            // if ( 1 & cntns )
            // FUDO| what about continuous correlation function???
            if ( cntns )
                ctmp = ctmp * cdata[tt0];
            else
                ctmp = ( a0 - mean ) * ( cdata[tt0] - cmean );

            acf[t] = acf[t] + ctmp;
            // nrm[t] = nrm[t] + 1.0;

            /* can we do this with integers? what exactly will happen, when we convert floats to integers? */
            /* currently the below will fail, because not everything beyond break-point will be assigned correctly... */

            if ( ( ctmp < ctff ) && ( cntns ) ) {
                // int ttmp;
                // printf("hello\n");
                // for ( ttmp=tt0; tt0max; ttmp++ ) {
                //     t = ttmp - t0;
                //     nrm[t] += 1;
                // }
                break;
            }

        }

    }

    /*
     * real sqvar;
     * real tmpnrm;
     *
     * sqvar = sqrt ( var ) * sqrt( cvar );
     */

    /*
     * introduce individual normalization
     * but take care, because 0th entry could be zero (depending on what we're doing)
     */

    // nrm[0] = 1;

    for ( t=0; t<acfln; t++ ) {
        /* tmpnrm = nrm[t] * sqvar; */
        /* check here, integer division intented */
        /* other option is to generate norm externally */
        // nrm[t] += nblcks - ( t - 1 ) / stpln;
        // printf("%e\n", nrm[t]);
        acf[t] = acf[t] / nrm[t];
    }

    // FUDO| need to include the right thing for cross-correlations
    // FUCHECK| if the below version for cross-correlation is actually correct
    if ( statnrm > 1 ) {
        real tmpnrm;

        if ( crss )
            tmpnrm = sqrt(var) * sqrt(cvar);
        else
            tmpnrm = var;

        if ( statnrm > 2 )
            if ( crss )
                tmpnrm /= sqrt ( fabsf ( mean * cmean ) );
            else
                tmpnrm /= fabsf ( mean );

        for ( t=0; t<acfln; t++ )
                acf[t] = acf[t] / tmpnrm;
    }
}

void corfun_fft ( real * acf, int acfln, real * data, real * cdata, int nstps, int padd, int statnrm, int nchnk )
{
    int stpln;
    int t0, tt0, t;
    int tmp, i;
    int tt0max;
    int fftln;
    int datln;
    int crss = 0;

    fftw_plan plan_forward;
    fftw_plan plan_backward;
    fftw_complex * fwout;
    fftw_complex * crssfwout;
    fftw_complex * bwin;
    real * bwout;

    real a0;

    real * dmn;
    real * cdmn;

    // FUDO| change naming nstps and datln this way is confusing
    int cpyln = nstps / nchnk;

    if ( padd )
        datln = 2 * nstps;
    else
        datln = nstps;

    if ( data != cdata )
        crss = 1;

    // FUDO| check here, datln is too much for this array, nstps should be fine
    // FUDO| this leads to fails, when statnrm is not used
    // FUDO| for now we'll just always allocate and deallocate it
    // if ( statnrm ) {
    dmn = ( real * ) malloc ( nstps * sizeof ( real ) );
    copy_real_array_1d ( dmn, data, nstps );

    if ( crss ) {
        cdmn = ( real * ) malloc ( nstps * sizeof ( real ) );
        copy_real_array_1d ( cdmn, cdata, nstps );
    }
    else
        cdmn = dmn;
    // }
    // else {
    //     dmn = data;
    //     if ( crss )
    //         cdmn = cdata;
    // }

    real mean = 0.;
    real cmean = 0.;
    real var = 1.;
    real cvar = 1.;

    // FUCHECK| for calculation of mean/variance/subtraction we leave only the actual data and not the padded part
    if ( statnrm ) {
        mean = get_mean ( dmn, nstps );
        var = get_variance_with_mean ( dmn, nstps, mean );

        subtract_value ( dmn, nstps, mean );

        if ( crss ) {

            cmean = get_mean ( cdmn, nstps );
            cvar = get_variance_with_mean ( cdmn, nstps, mean );

            subtract_value ( cdmn, nstps, cmean );

        }
        else {
            cmean = mean;
            cvar = var;
        }
    }

    datln /= nchnk;

    // FU| this is not the source of my problems, but nevertheless removed stack-allocation
    real * tmpacf = (real*) calloc ( datln, sizeof (real) );
    real * wrk = (real*) calloc ( datln, sizeof (real) );
    real * cwrk = (real*) calloc ( datln, sizeof (real) );

    for ( i=0; i<acfln; i++ )
        acf[i] = ZERO;

    /* check here and maybe reduce it by an given input... */

    fftln = floor ( datln / 2 ) + 1;

    int c;

    // FUDO| check if we need to re-allocate and free all the time to not mess up the FFT
    fwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );
    if ( crss )
        crssfwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );

    bwin = fftw_malloc ( sizeof ( fftw_complex ) * fftln );

    // FUDO| copy data into temporary array that we can padd individually?
    for ( c=0; c<nchnk; c++ ) {

        // FUDO| if not padded than just use a pointer, and do not explicitely copy the data
        copy_data_arrays_real_1d ( wrk, &(dmn[c*cpyln]), cpyln );
        copy_data_arrays_real_1d ( cwrk, &(cdmn[c*cpyln]), cpyln );

        /* verify somehow that the complex part is always zeros */

        plan_forward = fftw_plan_dft_r2c_1d ( datln, wrk, fwout, FFTW_ESTIMATE );

        fftw_execute ( plan_forward );

        if ( crss ) {
            fftw_destroy_plan ( plan_forward );
            plan_forward = fftw_plan_dft_r2c_1d ( datln, cwrk, crssfwout, FFTW_ESTIMATE );
            fftw_execute ( plan_forward );
        }

        /* produce square of absolute of complex numbers (square modulus) */
        /* find out why it does not matter that we don't scale it back before getting the square modulus */

        if ( crss )
            cmplx_conj_product ( bwin, fwout, crssfwout, fftln );
        else
            square_modulus ( bwin, fwout, fftln );

        plan_backward = fftw_plan_dft_c2r_1d ( datln, bwin, tmpacf, FFTW_ESTIMATE );

        fftw_execute ( plan_backward );

        // FUDO| maybe do things differently here
        if ( statnrm > 1 ) {
            real tmpnrm;

            if ( crss )
                tmpnrm = sqrt(var) * sqrt(cvar);
            else
                tmpnrm = var;

            if ( statnrm > 2 )
                if ( crss )
                    tmpnrm /= sqrt ( fabsf ( mean * cmean ) );
                else
                    tmpnrm /= fabsf ( mean );

            for ( t=0; t<acfln; t++ )
                    tmpacf[t] = tmpacf[t] / tmpnrm;
        }

        sum_acf ( acf, tmpacf, acfln );

        fftw_destroy_plan ( plan_forward );
        fftw_destroy_plan ( plan_backward );
    }

    // normalize_fftw ( tmpacf, datln );
    // FUDO| how about using datln as before? (more work?)
    for ( i=0; i<acfln; i++ )
        acf[i] /= (real) datln;

    free ( tmpacf );
    free ( wrk );
    free ( cwrk );

    divide_array_by_number_real_1d ( acf, (real) nchnk, cpyln );

    /* interestingly http://fy.chalmers.se/OLDUSERS/ahlstrom/fft_ac.pdf disagrees */
    if ( padd )
        normalize_acf_fft_padd ( acf, acfln );

    // real tval = acf[0];
    // for ( i=0; i<acfln; i++ )
    //     acf[i] /= tval;

    /* what if it's not padded? we should be fine, i think */

    fftw_free ( fwout );
    if ( crss )
        fftw_free ( crssfwout );
    fftw_free ( bwin );

    // if ( statnrm ) {
    free ( dmn );
    if ( crss )
        free ( cdmn );
    //}

    fftw_cleanup ();

}

/* read in data provided in file */
void read_data ( real **data, int dtrng, int nstps, char * txt, int offset, int strd, int mltpl_col )
{
    char rdln[MAXSTRLEN];
    char * vl;
    char * fname;

    FILE * fl;

    int i, j, k;

    fname = strtok ( txt, "\n");

    if ( !(( fl = fopen( txt, "r" ))) )
    {
        // printf("could not open '%s' file\n", txt);
        print_error ( FILE_NOT_FOUND, txt, __FILE__, __LINE__ );
    }

    for ( i=0; i<offset; i ++ ) {
        if ( fgets(rdln, MAXSTRLEN, fl) == NULL ) {
            // printf("Not enough data points in '%s'. Stopped at iteration %i\n", txt, i);
            print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );
        }
    }

    for ( i=0; i<nstps; i++ )
    {

        for ( k=0; k<strd; k++ )
            if ( fgets(rdln, MAXSTRLEN, fl) == NULL )
                print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );

        vl = strtok (rdln, " \n");

        for ( j=0; j<dtrng; j++ ) {
            if ( vl == NULL )
                print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );

            // FUDO| removing this right now, but would need to check again
            // if ( mltpl_col )
            data[j][i] = atof ( vl );
            vl = strtok (NULL, " \n");
        }

        // FUDO| removing this right now, but would need to check
        // if ( ! ( mltpl_col ) )
        //     data[0][i] = atof ( vl );
    }

    fclose(fl);
}

void read_data_fxd_fmt ( real * data, int dtrng, int nstps, char * txt, int offset, int btln_ntr, int strd )
{
    char rdln[MAXSTRLEN];
    char vl[MAXSTRLEN];
    char * fname;
    char text[MAXSTRLEN];
    char * dummy;

    int btln_ffst;
    int btln_ln;
    int btln_strd;
    int ncls;

    FILE * fl;

    int i, j;

    fname = strtok ( txt, "\n");

    if ( !(( fl = fopen( txt, "r" ))) )
    {
        // printf("could not open '%s' file\n", txt);
        print_error ( FILE_NOT_FOUND, txt, __FILE__, __LINE__ );
    }

    rewind(fl);
    fgets ( text, MAXSTRLEN, fl );
    btln_ln = strlen(text);

    dummy = strtok ( text, " \n" );
    ncls = 1;

    while ( dummy != NULL ) {
        dummy = strtok ( NULL, " \n" );
        ncls += 1;
    }

    // this does not work, because we don't actually count the spaces...
    // if ( btln_ln % ncls )
    //     print_error ( FATAL, "Your file does not seem to be suited for fixed format access.\n", __FILE__, __LINE__ );
    // btln_ntr = btln_ln / ncls;

    /* check here, jump over first #offset lines */
    /* this does not help, because later we are seeking from file beginning again */

    // if ( fseek ( fl, offset*btln_ln , SEEK_SET ) )
    //     print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );

    btln_ffst = offset * btln_ln;
    btln_strd = strd * btln_ln;

    for ( i=0; i<nstps; i++ )
    {
        /* jump to specific line */

        if ( fseek ( fl, btln_ffst+i*btln_strd , SEEK_SET ) )
            print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );

        /* jump to specific position in line */

        if ( dtrng )
            if ( fseek ( fl, (dtrng)*btln_ntr , SEEK_CUR ) )
                print_error ( NOT_ENOUGH_DATA, txt, __FILE__, __LINE__ );

        fread ( vl, btln_ntr , 1, fl );

        // printf("%s\n", vl);

        /* read next entry */

        data[i] = atof ( vl );
    }

    fclose(fl);
}

/* routine from allen/tildesley to calculate correlation function */

void read_input( int * nblcks, real * tstp, int * dtrng, int * nfls, int * nstps, int * padd, int * adjln, int * method, int * spec, int * read_acf, int * drvtv, int * crss, int * dtffst, real * thrmlz, int * cntns, int * slcd, real * ctff, int * fxdfmt, int * sclng, int * nrmlzd, int * ndvdlnrm, int * prntll, int * window, int * strd, int *statnrm, int *spcknd, int *fxdt, int *nchnk )
{
    FILE * datei;
    char * variable;
    char * value;
    char text[MAXSTRLEN];

    if ( !((datei = fopen("control", "r"))) )
    {
        // printf("could not open 'control' file\n");
        print_error ( FILE_NOT_FOUND, "control", __FILE__, __LINE__ );
    }

    while(fgets(text, MAXSTRLEN, datei) != NULL)
    {
        variable = strtok (text, " ");
        value = strtok (NULL, " ");

        if ( strstr(variable, "nblcks") != NULL )
        {
            *nblcks = atol(value);
        }
        else if ( strstr(variable, "tstp") != NULL )
        {
            *tstp = atof(value);
        }
        else if ( strstr(variable, "dtrng") != NULL )
        {
            *dtrng = atol(value);
        }
        else if ( strstr(variable, "nfls") != NULL )
        {
            *nfls = atol(value);
        }
        else if ( strstr(variable, "nstps") != NULL )
        {
            *nstps = atol(value);
        }
        else if ( strstr(variable, "padd") != NULL )
        {
            *padd = atol(value);
        }
        else if ( strstr(variable, "adjln") != NULL )
        {
            *adjln = atol(value);
        }
        else if ( strstr(variable, "method") != NULL )
        {
            if ( strstr ( value, "direct" ) != NULL )
                *method = DIRECT_ACF;
            else if ( strstr ( value, "fft" ) != NULL )
                *method = FFT_ACF;
            else if ( strstr ( value, "noacf" ) != NULL )
                *method = NO_ACF;
        }
        else if ( strstr(variable, "spec") != NULL )
        {
            *spec = atol(value);
        }
        else if ( strstr(variable, "read_acf") != NULL )
        {
            *read_acf = atol(value);
        }
        else if ( strstr(variable, "drvtv") != NULL )
        {
            *drvtv = atol(value);
        }
        else if ( strstr(variable, "crss") != NULL )
        {
            *crss = atol(value);
        }
        else if ( strstr(variable, "dtffst") != NULL )
        {
            *dtffst = atol(value);
        }
        else if ( strstr(variable, "thrmlz") != NULL )
        {
            *thrmlz = atof(value);
        }
        else if ( strstr(variable, "cntns") != NULL )
        {
            *cntns = atol(value);
        }
        else if ( strstr(variable, "slcd") != NULL )
        {
            *slcd = atol(value);
        }
        else if ( strstr(variable, "ctff") != NULL )
        {
            *ctff = atof(value);
        }
        else if ( strstr(variable, "fxdfmt") != NULL )
        {
            *fxdfmt = atol(value);
        }
        else if ( strstr(variable, "sclng") != NULL )
        {
            *sclng = atol(value);
        }
        else if ( strstr(variable, "nrmlzd") != NULL )
        {
            *nrmlzd = atol(value);
        }
        else if ( strstr(variable, "ndvdlnrm") != NULL )
        {
            *ndvdlnrm = atol(value);
        }
        else if ( strstr(variable, "prntll") != NULL )
        {
            *prntll = atol(value);
        }
        else if ( strstr(variable, "window") != NULL )
        {
            *window = atol(value);
        }
        else if ( strstr(variable, "strd") != NULL )
        {
            *strd = atol(value);
        }
        else if ( strstr(variable, "statnrm") != NULL )
        {
            *statnrm = atol(value);
        }
        else if ( strstr(variable, "spcknd") != NULL )
        {
            if ( strstr(value, "raman_iso") != NULL )
                *spcknd = RAMAN_ISO;
            else if ( strstr(value, "raman_ani") != NULL )
                *spcknd = RAMAN_ANI;
            else if ( strstr(value, "raman_san") != NULL )
                *spcknd = RAMAN_SAN;
            else if ( strstr(value, "infrared") != NULL )
                *spcknd = INFRARED;
            else
                *spcknd = GENERIC;
        }
        else if ( strstr(variable, "fxdt" ) != NULL )
        {
            *fxdt = atol(value);
        }
        else if ( strstr(variable, "nchnk" ) != NULL )
        {
            *nchnk = atol(value);
        }

    }

    fclose(datei);

}

void necessary_input_manipulations ( int * spcknd, int * dtrng, int * ncmpnt )
{
    switch ( *spcknd ) {
        case GENERIC:
            *dtrng = *dtrng; // * GENERIC;
            *ncmpnt = *dtrng;
            break;
        case INFRARED:
            *dtrng = 3;
            *ncmpnt = *dtrng;
            break;
        case RAMAN_ISO:
            *dtrng = 9;
            *ncmpnt = 1;
            break;
        case RAMAN_ANI:
            *dtrng = 9;
            *ncmpnt = 9;
            break;
        case RAMAN_SAN:
            *dtrng = 9;
            *ncmpnt = 9;
            break;
        default:
            *dtrng = *dtrng * GENERIC;
            *ncmpnt = *dtrng;
            break;
    }

}

void sum_acf ( real * acf, real * tmpacf, int len)
{
    int i;

    for ( i=0; i<len; i++ )
        acf[i] += tmpacf[i];

    return;
}

void output_acf ( real * acf, int acfln, real tstp )
{
    FILE * fout;
    int i;

    if ( !((fout = fopen ( "acf.dat", "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "acf.dat", __FILE__, __LINE__ );
    }

    for ( i=0; i<acfln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", i*tstp, acf[i]);

    fclose(fout);
}

void output_acf_numbered ( real * acf, int acfln, real tstp, int num )
{
    FILE * fout;
    int i;
    char fname[MAXSTRLEN];

    sprintf ( fname, "acf-%05i.dat", num );

    if ( !((fout = fopen ( fname, "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "acf.dat", __FILE__, __LINE__ );
    }

    for ( i=0; i<acfln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", i*tstp, acf[i]);

    fclose(fout);
}

void output_acf_titled ( real * acf, int acfln, real tstp, char *ttl )
{
    FILE * fout;
    int i;
    char fname[MAXSTRLEN];

    sprintf ( fname, "acf_%s.dat", ttl );

    if ( !((fout = fopen ( fname, "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "acf.dat", __FILE__, __LINE__ );
    }

    for ( i=0; i<acfln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", i*tstp, acf[i]);

    fclose(fout);
}

void normalize_acf ( real * acf, int acfln )
{
    int i;
    real a0;

    a0 = acf[0];

    /* get proper normalization with respect to average and stddev, see TODO file*/
    for ( i=0; i<acfln; i++ )
        acf[i] /= a0;

}

void normalize_acf_nfls ( real * acf, int len, int nfls )
{
    int i;
    real nrm;

    nrm = ( real ) nfls;

    /* get proper normalization with respect to average and stddev, see TODO file*/
    for ( i=0; i<len; i++ )
        acf[i] /= nrm;

}

void square_modulus ( fftw_complex * out, fftw_complex * in, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i][0] = sqr ( in[i][0] ) + sqr ( in[i][1] );
        out[i][1] = ZERO;
    }

}

void square_modulus_real ( real * out, fftw_complex * in, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i] = sqr ( in[i][0] ) + sqr ( in[i][1] );
    }

}

void cmplx_conj_product ( fftw_complex * out, fftw_complex * in1, fftw_complex * in2, int len )
{
    int i;

    for ( i=0; i<len; i++ ) {
        out[i][0] = in1[i][0] * in2[i][0] + in1[i][1] * in2[i][1];
        out[i][1] = in1[i][0] * in2[i][1] - in2[i][0] * in1[i][1];
        // printf("%f\n", out[i][1]);
    }

}

void normalize_fftw ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] /= len;

}

void collect_norm ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] += 1.;

}

void normalize_acf_fft_padd ( real * data, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] /= ( len - i );

}

void sum_fft ( fftw_complex * fft, fftw_complex * tmpfft, int len)
{
    int i;

    for ( i=0; i<len; i++ ) {
        fft[i][0] += tmpfft[i][0];
        fft[i][1] += tmpfft[i][1];
    }

    return;
}

void get_spectrum ( fftw_complex * mpltd, real * acf, int acfln )
{
    int i;
    int fftln;

    fftw_complex * fwout;
    fftw_plan plan_forward;

    fftln = floor ( acfln / 2 ) + 1;
    // fwout = fftw_malloc ( sizeof ( fftw_complex ) * fftln );

    plan_forward = fftw_plan_dft_r2c_1d ( acfln, acf, mpltd, FFTW_ESTIMATE );

    fftw_execute ( plan_forward );

    // for ( i=0; i<fftln; i++ ) {
    //     mpltd[i] = fwout[i][0];
    // }

    fftw_destroy_plan ( plan_forward );
    // fftw_free ( fwout );
    fftw_cleanup ();

}

void output_spectrum ( fftw_complex * mpltd, int spcln, real dt, real thrmlz )
{
    int i;
    FILE * fout;

    real fs;
    real df;

    fs = .5 / dt;
    df = fs / spcln;

    if ( !((fout = fopen ( "spectrum.dat", "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "spectrum.dat", __FILE__, __LINE__ );
    }

    if ( thrmlz > 0.0 ) {
        real wvn2frq;

        wvn2frq = SOL * 100.;

        for ( i=0; i<spcln; i++ )
            mpltd[i][0] *= ( 1. - exp ( -1. / KBOLTZ / thrmlz * i * df * PLANCK ) );
    }

    for ( i=0; i<spcln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", ( real ) i * df / SOL / 100. , mpltd[i][0]);
        // fprintf(fout, "%20.10e\n", mpltd[i]);

    fclose(fout);
}

void output_spectrum_real ( real * mpltd, int spcln, real dt, real thrmlz )
{
    int i;
    FILE * fout;

    real fs;
    real df;

    fs = .5 / dt;
    df = fs / spcln;

    if ( !((fout = fopen ( "spectrum.dat", "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "spectrum.dat", __FILE__, __LINE__ );
    }

    if ( thrmlz > 0. ) {
        real wvn2frq;

        wvn2frq = SOL * 100.;

        for ( i=0; i<spcln; i++ )
            mpltd[i] *= ( 1. - exp ( -1. / KBOLTZ / thrmlz * i * df * PLANCK ) );
    }

    for ( i=0; i<spcln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", ( real ) i * df / SOL / 100. , mpltd[i]);
        // fprintf(fout, "%20.10e\n", mpltd[i]);

    fclose(fout);
}

void drvtv_fv_pnt ( real * drv, real * data, int len, real h )
{
    int i;
    int fvp;

    real dt;

    /* check here if we need to do something about the units */
    /* this has an influence on the absolute intensities (not on the relative ones */
    dt = h / 1.E-15;

    fvp = len - 2;

    for ( i=2; i<fvp; i++ ) {
        drv[i] = -1. * data[i+2] + 8. * data[i+1] - 8. * data[i-1] + data[i-2];
        drv[i] /= ( 12. * dt );
        // drv[i] /= 12.;
    }

    drv[1] = data[2] - data[0];
    drv[1] /= ( 2. * dt );
    // drv[1] /= 2.;

    drv[len-2] = data[len-1] - data[len-3];
    drv[len-2] /= ( 2. * dt );
    // drv[len-2] /= 2.;

    drv[0] = data[1] - data[0];
    drv[0] /=  dt;
    // drv[0] /= 1;

    drv[len-1] = data[len-1] - data[len-2];
    drv[len-1] /= dt;
    // drv[len-1] /= 1;

}

void blocked_squared_spectrum ( real * spcout, int acfln, int nblcks, real * data, int datln )
{
    int i;
    int cntfft = 0;
    int blckln;
    int fft_blckln;
    int tmpln;

    real tmpout[acfln];
    fftw_complex * fwout;
    fftw_plan plan_forward;

    blckln = floor ( datln / nblcks );
    fft_blckln = floor ( blckln / 2 ) + 1;

    fwout = fftw_malloc ( sizeof ( fftw_complex ) * fft_blckln );

    /* check here and add individual padding? (dunt make no sense, IMHO) */
    for ( i=0; i<acfln; i++ )
        tmpout[i] = ZERO;

    for ( i=0; i<datln; i += blckln ) {
        tmpln = datln - i;

        if ( tmpln < blckln )
            break;

        plan_forward = fftw_plan_dft_r2c_1d ( blckln, &(data[i]), fwout, FFTW_ESTIMATE );

        fftw_execute ( plan_forward );

        square_modulus_real ( tmpout, fwout, acfln );

        sum_acf ( spcout, tmpout, acfln );

        fftw_destroy_plan ( plan_forward);

        cntfft++;

    }

    fftw_free ( fwout );
    fftw_cleanup ();

    for ( i=0; i<acfln; i++ )
        spcout[i] /= ( real ) cntfft;

}

real get_mean ( real * data, int len )
{
    int i;

    real mean = ZERO;

    for ( i=0; i<len; i++ )
        mean += data[i];

    mean /= ( real ) len;

    return mean;
}

void subtract_value ( real * data, int len, real val )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] -= val;
}

real get_variance ( real * data, int len )
{
    int i;
    real msq = ZERO;
    real var;
    real mean;

    mean = get_mean ( data, len );

    for ( i=0; i<len; i++ )
        msq += sqr ( data[i] );

    msq /= ( real ) len;

    var = msq - sqr ( mean );

    return var;
}

real get_variance_with_mean ( real * data, int len, real mean )
{
    int i;
    real var = ZERO;

    for ( i=0; i<len; i++ )
        var += pow ( ( data[i] - mean ), 2.0 ) / len;

    return var;
}

// FUDO| this one suffers from catastrophic cancellation
real get_variance_with_mean_stupid ( real * data, int len, real mean )
{
    int i;
    real msq = ZERO;
    real var;

    for ( i=0; i<len; i++ )
        msq += sqr ( data[i] );

    msq /= ( real ) len;

    var = msq - sqr ( mean );

    return var;
}

void generate_nrm ( real * nrm, int nrmln, int nstps, int nblcks, int acfln )
{

    int i;
    int stpln, t, t0, tt0, tcor, tmp, tt0max;

    stpln = nstps / nblcks;
    tcor = acfln;

    /* check here if acfln is the right ending for the norm */
    /* and also confirm that norm is generated correctly, although I think I did that */

    for ( i=0; i<acfln; i++ )
        nrm[i] = ZERO;

    for ( t0=0; t0<nstps; t0 += stpln ) {
        tmp = t0 + tcor;
        tt0max = min( nstps, tmp );

        for ( tt0=t0; tt0<tt0max; tt0++ ) {
            t = tt0 - t0;
            nrm[t] = nrm[t] + 1.0;
        }
    }

}

void scale_data ( real * data, int len, real scale )
{
    int i;

    for ( i=0; i<len; i++ )
        data[i] *= scale;
}

void check_malformed_input ( int nblcks, real tstp, int dtrng, int nfls, int nstps, int padd, int adjln, int method, int spec, int read_acf, int drvtv, int crss, int dtffst, real thrmlz, int cntns, int slcd, real ctff, int fxdfmt, int sclng, int nrmlzd, int ndvdlnrm, int prntll, int window, int statnrm, int spcknd, int nchnk )
{

    if ( ( slcd ) && ( !( adjln ) ) )
    {
        print_error(MISSING_INPUT_PARAM, "adjln", __FILE__, __LINE__);
    }

    if ( ( slcd ) && ( method == FFT_ACF ) )
    {
        print_error(NOT_IMPLEMENTED, "Sliced input plus FFT method", __FILE__, __LINE__);
    }

    if ( ( slcd ) && ( method == NO_ACF ) )
    {
        print_error(NOT_IMPLEMENTED, "Sliced input plus NO_ACF method", __FILE__, __LINE__);
    }

    if ( ( cntns ) && ( method == FFT_ACF ) )
    {
        print_error(NOT_IMPLEMENTED, "Continuous correlation function with FFT", __FILE__, __LINE__);
    }

    if ( ( cntns ) && ( method == NO_ACF ) )
    {
        print_error( NONSENSICAL, "Calculating continuous correlation function without calculating correlation function", __FILE__, __LINE__);
    }

    if ( ( padd ) && ( method == DIRECT_ACF ) )
    {
        print_warning ( MEMORY_WASTE, "padding is not used in DIRECT calculation" );
    }

    if ( ! ( padd ) && ( method == FFT_ACF ) )
    {
        print_warning ( YOU_KNOW_WHAT, "Input padding is generally recommended with FFT method, but you probably know what you're doing" );
    }

    if ( ( adjln ) && ( method == FFT_ACF ) )
    {
        // check here, maybe a NOT_IMPLEMENTED error would be better
        print_warning ( NO_EFFECT, "Adjusting lengths while striding over data with FFT data" );
    }

    if ( !( nstps ) )
    {
        print_error ( MISSING_INPUT_PARAM, "nstps" , __FILE__, __LINE__);
    }

    if ( !( nfls ) )
    {
        print_error ( MISSING_INPUT_PARAM, "nfls", __FILE__, __LINE__ );
    }

    if ( (cntns) && ( ctff == -10000. ) )
    {
        print_warning ( YOU_KNOW_WHAT, "Providing a cutoff for continuous correlation function can significantly speed up things\n" );
    }

    if ( ( !(cntns) ) && ( ctff != -10000. ) )
    {
        print_warning ( YOU_KNOW_WHAT, "'ctff' has only an effect on continuous correlation functions\n" );
    }
    if ( ( window ) && ( method == NO_ACF) )
    {
        print_error ( NOT_IMPLEMENTED, "Window functions in frequency space", __FILE__, __LINE__);
    }
    if ( ( statnrm ) && ( cntns ) )
    {
        print_error(NOT_IMPLEMENTED, "Continuous correlation function with statistically correct normalization", __FILE__, __LINE__);
    }
    if ( ( spcknd != GENERIC ) && ( dtrng != 1 ) )
    {
        print_warning ( YOU_KNOW_WHAT, "'dtrng' parameter is overwritten, because other than generic spectra are to be produced\n" );
    }
    if ( ( nchnk > 1 ) && ( padd ) )
    {
        // print_error ( NOT_IMPLEMENTED, "Padding and chunk-dividing", __FILE__, __LINE__);
        print_warning ( EXPFEATURE, "Padding in combination with chunk-dividing" );
    }
    if ( ( nchnk > 1 ) && ( nblcks > 1 ) )
    {
        print_error ( NOT_IMPLEMENTED, "Block- and chunk-dividing", __FILE__, __LINE__);
    }
    if ( ( spcknd == RAMAN_ISO ) && ( window ) && !( statnrm ) )
    {
        print_error ( NONSENSICAL, "Window function plus isotropic Raman spectrum calculation without statnrm", __FILE__, __LINE__);
    }
    /* FUDO| for sanity's sake, why should that be?
    if ( ( window ) && !( statnrm ) )
    {
        print_error ( NONSENSICAL, "Window function without statnrm", __FILE__, __LINE__);
    }
    */

#ifndef EXPERIMENTAL
    if ( sclng )
    {
        print_error ( EXPCODE, "Scaling", __FILE__, __LINE__ );
    }
#endif

}

void apply_window_function ( real *out, int window, real *inp, int datln )
{

    switch ( window ) {
        case BLACKMAN:
            printf("Using a Blackman-Harris window.\n");
            apply_blackman_window ( out, inp, datln );
            break;
        case UNIFORM_RECTANGULAR:
            printf("Using a uniform rectangular window (nothing to do).\n");
            break;
        default:
            break;
    }
}

void apply_blackman_window ( real *out, real *inp, int datln )
{
    int i, tmpi;
    int dtmo, hlf_datln;
    real fct, dbl_fct;
    real a0, a1, a2;

    a0 = 0.42;
    a1 = 0.50;
    a2 = 0.08;

    dtmo = datln - 1;
    fct = 2. * PI / dtmo;
    dbl_fct = 2. * fct;

    hlf_datln = datln / 2;

    real bh_wndw[datln];

    /* from NumPy:     return 0.42 - 0.5*cos(2.0*pi*n/(M-1)) + 0.08*cos(4.0*pi*n/(M-1)) */

    // FUDO| blackman-harris centered around middle of correlation function
    // for ( i=0; i<datln; i++ )
    //     out[i] = inp[i] * ( a0 - a1 * cos ( fct * i ) + a2 * cos ( dbl_fct * i ) );

    for ( i=0; i<datln; i++ ) {
        tmpi = i + hlf_datln;
        bh_wndw[i] = ( a0 - a1 * cos ( fct * tmpi ) + a2 * cos ( dbl_fct * tmpi ) );
        out[i] = inp[i] * bh_wndw[i];
    }

    output_data_titled ( bh_wndw, datln, 1.0, "window");

    // for ( i=hlf_datln; i<datln; i++ )
    //    out[i] = 0.;
}

void copy_real_array_1d ( real *out, real *in, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        out[i] = in[i];
}

void prep_data_for_correlation ( real ** data, real ** cdata, int datln, int spcknd, int crss )
{
    int i;

    if ( ( spcknd == GENERIC ) || ( spcknd == INFRARED ) )
        return;

    // FUNFO| polarizability tensor should be in the following format a_xx, a_xy, a_xz, a_yx, a_yy, a_ayz, a_zx, a_zy, a_zz
    // FUNFO|                                                           0     1     2     3     4     5      6     7     8

    real *iso = ( real * ) malloc ( datln * sizeof ( real ) );

    if ( spcknd == RAMAN_ISO ) {

        get_isotropic_alpha ( iso, data, datln );

        for ( i=0; i<datln; i++ )
            data[0][i] = iso[i];

        if ( crss ) {
            get_isotropic_alpha ( iso, cdata, datln );

            for ( i=0; i<datln; i++ )
                cdata[0][i] = iso[i];
        }
    }
    else if ( ( spcknd == RAMAN_ANI ) || ( spcknd == RAMAN_SAN ) ) {

        // FUDO| maybe use a constant here for assigning how many entries the two-dimensional array should have
        // FUDO| if we wanna avoid re-reading the same data twice for non-cross calculations, we should reassign here the data from data to cdata
        // FUDO|

        get_isotropic_alpha ( iso, data, datln );

        for ( i=0; i<datln; i++ ) {
            data[0][i] -= iso[i];
            data[4][i] -= iso[i];
            data[8][i] -= iso[i];
        }

        get_isotropic_alpha ( iso, cdata, datln );

        for ( i=0; i<datln; i++ ) {
            cdata[0][i] -= iso[i];
            cdata[4][i] -= iso[i];
            cdata[8][i] -= iso[i];
        }
        // FUNFO|| for RAMAN_SAN everything should be fine until here

        if ( spcknd == RAMAN_ANI ) {

            if ( crss ) {
                real tmp;
                // a_xx, a_yy, a_zz stay the same, only off-diagonal elements are interchanged
                for ( i=0; i<datln; i++ ) {
                    tmp = cdata[1][i];
                    cdata[1][i] = cdata[3][i];
                    cdata[3][i] = tmp;

                    tmp = cdata[5][i];
                    cdata[5][i] = cdata[7][i];
                    cdata[7][i] = tmp;

                    tmp = cdata[2][i];
                    cdata[2][i] = cdata[6][i];
                    cdata[6][i] = tmp;

                }
            }
            else {
                for ( i=0; i<datln; i++ ) {
                    cdata[1][i] = data[3][i];
                    cdata[3][i] = data[1][i];

                    cdata[5][i] = data[7][i];
                    cdata[7][i] = data[5][i];

                    cdata[2][i] = data[6][i];
                    cdata[6][i] = data[2][i];
                }
            }
        }
    }

    // FUCHECK| at this point we should have read the data from the file (also the second time)
    // FUDO| could also re-assign the data here, then we don't need to read the file twice... (but we would need to modify a couple of lines above as well)

    free ( iso );

}

void get_isotropic_alpha ( real * iso, real ** data, int datln )
{
    int i;

    for ( i=0; i<datln; i++ ) {
        iso[i] = ( data[0][i] + data[4][i] + data[8][i] ) / 3.;
        // if ( iso[i] < 0 )
        //     iso[i] *= -1.;
    }

}

int fix_data ( real * data, int datln )
{
    int i;
    real av = ZERO;

    int cnt = 0;
    for ( i=1; i<datln-1; i++ ) {
        av += fabsf ( data[i] );
        if ( fabsf ( data[i] ) > 10*av/i ) {
            av -= data[i];
            data[i] = ( data[i+1] + data[i-1] ) / 2.;
            // printf("FIXING DATA ENTRY %i %f %f\n", i, data[i+1], data[i-1]);
            av += data[i];
            cnt++;
        }
    }

    return cnt;
}

void output_data_titled ( real * dat, int datln, real tstp, char *ttl )
{
    FILE * fout;
    int i;
    char fname[MAXSTRLEN];

    sprintf ( fname, "dat_%s.dat", ttl );

    if ( !((fout = fopen ( fname, "w" ))) )
    {
        print_error ( FILE_NOT_FOUND, "dat.dat", __FILE__, __LINE__ );
    }

    for ( i=0; i<datln; i++ )
        fprintf(fout, "%20.10e %20.10e\n", i*tstp, dat[i]);

    fclose(fout);
}

void zero_array_real_1d ( real * array, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        array[i] = ZERO;
}

void add_arrays_real_1d ( real *out, real *in1, real *in2, int len)
{
    int i;

    for ( i=0; i<len; i++ )
        out[i] = in1[i] + in2[i];
}

void divide_array_by_number_real_1d ( real * array, real number, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        array[i] /= number;
}

void copy_data_arrays_real_1d ( real *out, real *in, int len )
{
    int i;

    for ( i=0; i<len; i++ )
        out[i] = in[i];
}
