/*

SPEX is a C program to calculate correlation functions and spectra
Copyright 2013 Frank Uhlig (uhlig.frank@gmail.com)

This file is part of SPEX.

SPEX is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPEX is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPEX.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <fftw3.h>

typedef enum methods_e
{
    DIRECT_ACF=1,
    FFT_ACF,
    NO_ACF,
} methods_t;

typedef enum window_e
{
    UNIFORM_RECTANGULAR=0,
    BLACKMAN=1,
} window_t;

//FUDO| check here, this is prolly not the nicest way of doing things
typedef enum speckind_e
{
    GENERIC=1000,
    RAMAN_ISO,
    INFRARED,
    RAMAN_ANI,
    RAMAN_SAN,
    // RAMAN_POL,
    // RAMAN_DEPOL,
    // RAMAN_VV,
    // RAMAN_VH,
} speckind_t;

typedef struct spctrm_s
{
    int kind;
    int raman;
} spctrm_t;

// typedef float real;
typedef double real;
void read_data (real **data, int dtrng, int nstps, char * txt, int offset, int strd, int mltpl_col );
void read_data_fxd_fmt (real * data, int dtrng, int nstps, char * txt, int offset, int btln_ntr, int strd );
void read_input ( int * nblcks, real * tstp, int * dtrng, int * nfls, int * nstps, int * padd, int * adjln, int * method, int * spec, int * read_acf, int * drvtv, int * crss, int * dtffst, real * thrmlz, int * cntns, int * slcd, real * ctff, int * fxdfmt, int * sclng, int * nrlmzd, int * ndvdlnrm, int * prntll, int * window, int * strd, int * statnrm, int *spcknd, int *fxdt, int *nchnk );
void corfun_direct ( real * acf, int acfln, real * data, real * cdata, int nstps, int nblcks, int cntns, real * nrm, real ctff, int statnrm );
void corfun_fft (real * acf, int acfln, real * data, real * cdata, int nstps, int padd, int statnrm, int nchnk );
void necessary_input_manipulations ( int * spcknd, int * dtrng, int * ncmpnt );
void sum_acf ( real * acf, real * tmpacf, int len);
void output_acf ( real * acf, int acfln, real tstp );
void output_acf_numbered ( real * acf, int acfln, real tstp, int num );
void output_acf_titled ( real * acf, int acfln, real tstp, char *ttl );
void normalize_acf ( real * acf, int acfln );
void normalize_acf_nfls ( real * acf, int len, int nfls );
void copy_data ( real * out, real * in, int start, int len );
void square_modulus ( fftw_complex * out, fftw_complex * in, int len );
void square_modulus_real ( real * out, fftw_complex * in, int len );
void cmplx_conj_product ( fftw_complex * out, fftw_complex * in1, fftw_complex * in2, int len );
void normalize_fftw ( real * data, int len );
void collect_norm ( real * data, int len );
void normalize_acf_fft_padd ( real * data, int len );
void sum_fft ( fftw_complex * fft, fftw_complex * tmpfft, int len);
void print_fingers ();
void print_feet ( int lr );
void get_spectrum ( fftw_complex * mpltd, real * acf, int acfln );
void output_spectrum ( fftw_complex * mpltd, int spcln, real dt, real thrmlz );
void output_spectrum_real ( real * mpltd, int spcln, real dt, real thrmlz );
void drvtv_fv_pnt ( real * drv, real * data, int len, real h );
void blocked_squared_spectrum ( real * spcout, int acfln, int nblcks, real * data, int datln );
real get_mean ( real * data, int len );
void subtract_value ( real * data, int len, real val );
real get_variance ( real * data, int len );
real get_variance_with_mean ( real * data, int len, real mean );
void generate_nrm ( real * nrm, int nrmln, int nstps, int nblcks, int acfln );
void scale_data ( real * data, int len, real scale );
void check_malformed_input ( int nblcks, real tstp, int dtrng, int nfls, int nstps, int padd, int adjln, int method, int spec, int read_acf, int drvtv, int crss, int dtffst, real thrmlz, int cntns, int slcd, real ctff, int fxdfmt, int sclng, int nrmlzd, int ndvdlnrm, int prntll, int window, int statnrm, int spcknd, int nchnk );
void copy_real_array_1d ( real *out, real *in, int len );
void apply_window_function ( real *out, int window, real *inp, int datln );
void apply_blackman_window ( real *out, real *inp, int datln );
void prep_data_for_correlation ( real ** data, real ** cdata, int datln, int spcknd, int crss );
void get_isotropic_alpha ( real * iso, real ** data, int datln );
int fix_data ( real * data, int datln );
void output_data_titled ( real * dat, int datln, real tstp, char *ttl );
void zero_array_real_1d ( real * array, int len );
void add_arrays_real_1d ( real *out, real *in1, real *in2, int len);
void divide_array_by_number_real_1d ( real * array, real number, int len );
void copy_data_arrays_real_1d ( real *out, real *in, int len );
